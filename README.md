Simple 3d software renderer
=====================================================

The goal of this imgui app is to show how simple factors linked with camera/object/world properties impacts on Model, View and Projection matrices and how they finally affect rendered frame.
For educational purposes all draw operations are performed by raw access to array of pixels.

![Screenshot](https://bitbucket.org/80286/ndc/raw/master/res/241021_imgui.gif)

### How to build and launch

- `git clone https://bitbucket.org/80286/ndc.git && cd ndc`
- `git submodule update --init --recursive`
- download SDL >= 2.0.17 [snapshot](https://www.libsdl.org/tmp/SDL-2.0.17-ea97ab6.zip) from [here](https://www.libsdl.org/git.php) - this step is mandatory becuase `SDL_RenderGeometryRaw` is not present in last stable SDL version.

```
wget https://www.libsdl.org/tmp/SDL-2.0.17-ea97ab6.zip
unzip -d src/contrib SDL-2.0.17-ea97ab6.zip
```

- `mkdir build`
- `cd build && cmake ..`
- `make`
- `./ndc`


### References to used materials
[Song Ho Ahn - OpenGL Projection Matrix](http://www.songho.ca/opengl/gl_projectionmatrix.html)

[Song Ho Ahn - OpenGL Transformation](http://www.songho.ca/opengl/gl_transform.html)

[Benny Bobaganoosh (BennyQBD)](https://github.com/BennyQBD/3DSoftwareRenderer)

[Kenneth I. Joy - Clipping](http://fabiensanglard.net/polygon_codec/clippingdocument/Clipping.pdf)

[David Henry - MDL file format (Quake's models)](http://tfc.duke.free.fr/coding/mdl-specs-en.html)

[imgui](https://github.com/ocornut/imgui)

[imgui-cmake](https://github.com/Pesc0/imgui-cmake)
