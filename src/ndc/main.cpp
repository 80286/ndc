#include "apps/ImGuiApp.h"

int main(int argc, char *argv[]) {
    puts("main()");

    ImGuiApp app;

    app.Run(1280, 720);
    app.Quit();

    return 0;
}
