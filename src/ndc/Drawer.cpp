#include "Drawer.h"

Drawer::Drawer(uint8_t *data, const int width, const int height, const int pixelSize) {
    m_img = data;
    m_width = width;
    m_height = height;
    m_depthBuffer = new float[m_width * m_height];
    m_pixelSize = pixelSize;
}

Drawer::~Drawer(void) {
    delete m_depthBuffer;
}

void Drawer::drawPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b) {
    uint8_t *pixel = getPixelPtr(x, y);

    pixel[0] = m_defaultAlpha;
    pixel[1] = r;
    pixel[2] = g;
    pixel[3] = b;
}

void Drawer::clear(void) {
    const int img_len = m_pixelSize * (m_width * m_height);

    for(int i = 0; i < img_len; i += m_pixelSize) {
        m_img[i + 0] = 0;
        m_img[i + 1] = (uint8_t)0;
        m_img[i + 2] = (uint8_t)0;
        m_img[i + 3] = (uint8_t)0;
    }
}

float Drawer::getAspect(void) const {
    return ((float)m_width) / ((float)m_height);
}

uint8_t *Drawer::getData(void) {
    return m_img;
}

int Drawer::getWidth(void) const {
    return m_width;
}

int Drawer::getHeight(void) const {
    return m_height;
}

int Drawer::getPixelPos(int x, int y) {
    return m_pixelSize * ((y * m_width) + x);
}

uint8_t *Drawer::getPixelPtr(int x, int y) {
    return m_img + getPixelPos(x, y);
}

void Drawer::drawPixel(int x, int y, const RGB &rgb) {
    this->drawPixel(x, y, rgb.red(), rgb.green(), rgb.blue());
}

void Drawer::drawPoint(int x, int y, uint8_t r, uint8_t g, uint8_t b) {
    int i, j;

    for(j =  2; j > -2; j--) {
        for(i = -2; i < 3; i++) {
            drawPixel(x + i, y + j, r, g, b);
        }
        for(i = -2; i < 3; i++) {
            drawPixel(x + i, y - j, r, g, b);
        }
    }
}

void Drawer::drawPoint(int x, int y, const RGB &rgb) {
    drawPoint(x, y, rgb.red(), rgb.green(), rgb.blue());
}

void Drawer::drawPoint(Point point, const RGB &rgb) {
    drawPoint(point.x, point.y, rgb.red(), rgb.green(), rgb.blue());
}

bool Drawer::isInScreen(int x, int y) const {
    return (x >= 0 && x < m_width)
        && (y >= 0 && y < m_height);
}

void Drawer::drawLine(Point a, Point b, const RGB &rgb) {
    const int ax = (int)a.x;
    const int ay = (int)a.y;
    const int bx = (int)b.x;
    const int by = (int)b.y;

    drawLine(ax, ay, bx, by, rgb);
}

void Drawer::drawLine(int ax, int ay, int bx, int by, const RGB &rgb) {
    float slope;
    float dist;
    int x, y;

    /* printf("drawLine; A:(%3d, %3d), B:(%3d, %3d)\n", a.x, a.y, b.x, b.y); */
    /* if(!this->clipSegmentToScreen(a, b)) { */
        /* return; */
    /* } */

    /* printf("[FINAL] drawLine; A:(%3d, %3d), B:(%3d, %3d)\n", ax, ay, bx, by); */

    slope = (float)(by - ay) / (float)(bx - ax);

    if (fabs(slope) <= 1.0f) {
        if (ax > bx) {
            std::swap(ax, bx);
            std::swap(ay, by);
        }

        slope = (float)(by - ay) / (float)(bx - ax);
        dist = (float)ay;
        for (x = ax; x < bx; x++) {
            drawPixel(x, (int)dist, rgb);
            dist += slope;
        }
    } else {
        if (ay > by) {
            std::swap(ax, bx);
            std::swap(ay, by);
        }

        slope = (float)(bx - ax) / (float)(by - ay);
        dist = (float)ax;
        for (y = ay; y < by; y++) {
            drawPixel((int)dist, y, rgb);
            dist += slope;
        }
    }
}

void Drawer::drawHorizontalLine(int x1, int x2, int y, const RGB &rgb) {
    for(int x = x1; x < x2; ++x) {
        drawPixel(x, y, rgb);
    }
}

void Drawer::initDepthBuffer(void) {
    const int bufferLength = getHeight() * getWidth();

    for(int i = 0; i < bufferLength; ++i) {
        m_depthBuffer[i] = 99999.0f;
    }
}

void Drawer::drawWiredTriangle(
        Point p1, 
        Point p2, 
        Point p3, 
        const RGB &rgb) {
    drawLine(p1, p2, rgb);
    drawLine(p1, p3, rgb);
    drawLine(p2, p3, rgb);
}

void Drawer::fillHorizontalLine(
        Triangle *triangle, 
        Edge *leftEdge, 
        Edge *rightEdge,
        int y) {
    const int xmin = (int)ceilf(leftEdge->getX());
    const int xmax = (int)ceilf(rightEdge->getX());

    const float xPrestep = (xmin - leftEdge->getX());

    const float depthStep = triangle->getDepthStepX();
    const float lightAmountStep = triangle->getLightAmountStepX();

    float depth = leftEdge->getDepth() + xPrestep * depthStep;
    float lightAmount = leftEdge->getLightAmount() + xPrestep * lightAmountStep;

    int depthBufferIndex = getWidth() * y + xmin;

    for(int x = xmin; x < xmax; ++x) {
        if (depth < m_depthBuffer[depthBufferIndex]) {
            m_depthBuffer[depthBufferIndex] = depth;

            const uint8_t color = (uint8_t)(clamp(lightAmount, 0.1f, 1.0f) * 255.0f);
            drawPixel(x, y, color, color, color);
        }

        depthBufferIndex++;

        depth += depthStep;
        lightAmount += lightAmountStep;
    }
}

void Drawer::fillSubTriangle(
        Triangle *triangle, 
        Edge *shortEdge, 
        Edge *longEdge, 
        bool ymidOnRightSide) {
    Edge* leftEdge = shortEdge;
    Edge* rightEdge = longEdge;

    /* Ensure that left edge has smaller initial X value than right edge: */
    if(ymidOnRightSide) {
        std::swap(leftEdge, rightEdge);
    }

    const int ymin = (float)ceilf(shortEdge->getMinY());
    const int ymax = (float)ceilf(shortEdge->getMaxY());

    for(int y = ymin; y < ymax; ++y) {
        fillHorizontalLine(triangle, leftEdge, rightEdge, y);
        leftEdge->step();
        rightEdge->step();
    }
}

void sortPointsByY(Point **a, Point **b, Point **c) {
    if ((*a)->y >= (*c)->y) {
        std::swap(*a, *c);
    }
    if ((*a)->y >= (*b)->y) {
        std::swap(*a, *b);
    }
    if ((*b)->y >= (*c)->y) {
        std::swap(*b, *c);
    }
}

int pointOnSide(Point *a, Point *b, Point *c) {
    return (c->x - a->x) * (b->y - a->y) - (c->y - a->y) * (b->x - a->x);
}

void Drawer::fillTriangle(Point &a, Point &b, Point &c) {
    Point *ymin, *ymid, *ymax;
    bool ymidOnRightSide;

    ymin = &a;
    ymid = &b;
    ymax = &c;

    sortPointsByY(&ymin, &ymid, &ymax);

    Triangle triangle(ymin, ymid, ymax);

    Edge longEdge(&triangle, *ymin, *ymax, 0);
    Edge topEdge(&triangle, *ymin, *ymid, 0);
    Edge bottomEdge(&triangle, *ymid, *ymax, 1);

    ymidOnRightSide = (pointOnSide(ymin, ymax, ymid) > 0);

    fillSubTriangle(&triangle, &topEdge, &longEdge, ymidOnRightSide);
    fillSubTriangle(&triangle, &bottomEdge, &longEdge, ymidOnRightSide);
}
