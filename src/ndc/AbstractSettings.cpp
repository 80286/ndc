#include "AbstractSettings.h"

void AbstractSettings::updateProjectionMatrix() {
    if(isGlFrustumEnabled()) {
        m_projectionMatrix.setFrustum(
                frustum_left(),
                frustum_right(),
                frustum_bottom(),
                frustum_top(),
                frustum_near(),
                frustum_far());
    } else {
        m_projectionMatrix.setPerspective(
                frustum2_fov(),
                frustum2_aspect(),
                frustum2_near(),
                frustum2_far());
    }
}

void AbstractSettings::updateViewMatrix() {
    m_viewMatrix.loadIdentity();
    m_viewMatrix.translate(-view_tx(), -view_ty(), -view_tz());
    m_viewMatrix.rotateZ(-view_rotz());
    m_viewMatrix.rotateY(-view_roty());
    m_viewMatrix.rotateX(-view_rotx());
}

void AbstractSettings::updateModelMatrix() {
    m_modelMatrix.loadIdentity();
    m_modelMatrix.rotateZ(model_rotz());
    m_modelMatrix.rotateY(model_roty());
    m_modelMatrix.rotateX(model_rotx());
    m_modelMatrix.scaleXYZ (model_sx(), model_sy(), model_sz());
    m_modelMatrix.translate(model_tx(), model_ty(), model_tz());
}
