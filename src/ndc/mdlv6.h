#ifndef MDLV6_H
#define MDLV6_H

#include "stdlib.h"
#include "stdint.h"
#include "math/c/v3.h"

#if __cplusplus
extern "C" {
#endif

struct texture_t {
    int w;
    int h;
    uint8_t *data;
};

/* Header: */
struct mdlv6_header_t {
    int32_t ident;
    int32_t version;

    v3 scale;
    v3 translation;
    float bounding_radius;
    v3 eye_position;

    int32_t skins_len;
    int32_t skin_width;
    int32_t skin_height;

    int32_t vertices_len;
    int32_t triangles_len;
    int32_t frames_len;

    int32_t sync_type;
    int32_t flags;
    float size;
};

/* Texture information: */
struct mdlv6_skin_t {
    int32_t group;
    uint8_t *data;
};

struct mdlv6_groupskin_t {
    int32_t group;
    int32_t time_len;
    float *time;
    uint8_t **data;
};

struct mdlv6_texcoord_t {
    int32_t on_seam;
    int32_t s;
    int32_t t;
};

/* Vertices: */
struct mdlv6_vertex_t {
    /* (x, y, z): */
    uint8_t v[3];
    /* Index of one of 162 normal vectors: */
    uint8_t normal_index;
};

/* Triangles: */
struct mdlv6_triangle_t {
    int32_t front;
    int32_t vertex[3];
};

/* Frames: */
struct mdlv6_simple_frame_t {
    struct mdlv6_vertex_t bboxmin;
    struct mdlv6_vertex_t bboxmax;
    int8_t name[16];
    struct mdlv6_vertex_t *vertices;
};

struct mdlv6_frame_t {
    int32_t type;
    struct mdlv6_simple_frame_t frame;
};

struct mdlv6_groupframe_t {
    int32_t type;
    struct mdlv6_vertex_t min;
    struct mdlv6_vertex_t max;
    float *time;
    struct mdlv6_simple_frame_t *frames;
};

/* Main structure: */
struct mdlv6_t {
    struct mdlv6_header_t header;

    struct mdlv6_skin_t     *skins;
    struct mdlv6_texcoord_t *texcoords;
    struct mdlv6_triangle_t *triangles;
    struct mdlv6_frame_t    *frames;

    struct texture_t **textures;
    int32_t iskin;
};

void mdlv6_init(struct mdlv6_t* mdl);
int  mdlv6_readfile(const char *filename, struct mdlv6_t *mdl);
void mdlv6_transformVertex(
        float [3], 
        const struct mdlv6_header_t *, 
        const struct mdlv6_vertex_t *);
void mdlv6_free(struct mdlv6_t *mdl);
void mdlv6_getNormal(v3, int);

#if __cplusplus
}
#endif

#endif
