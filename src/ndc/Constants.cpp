#include "Constants.h"

static const std::map<GluPerspectiveType, FloatField> GluPerspectiveConstants = {
    {GPT_FOVY,      {0.0f,    3.14f,   0.2f, "fovy"}},
    {GPT_ASPECT,    {0.0f,    3.14f,   1.3f, "aspect"}},
    {GPT_NEAR,      {1.0f, 1000.0f,    1.0f, "near"}},
    {GPT_FAR,       {1.0f, 1000.0f,  300.0f, "far"}}
};

static const std::map<FrustumType, FloatField> Frustum = {
    {FT_LEFT,      {-10.0f, 10.0f, -0.5f, "left"}},
    {FT_RIGHT,     {-10.0f, 10.0f,  0.5f, "right"}},
    {FT_BOTTOM,    {-10.0f, 10.0f,  0.5f, "bottom"}},
    {FT_TOP,       {-10.0f, 10.0f, -0.5f, "top"}},
    {FT_NEAR,      {1.0f, 1000.0f,   1.0f, "near"}},
    {FT_FAR,       {1.0f, 1000.0f, 300.0f, "far"}}
};

static const std::map<ModelType, FloatField> ModelConstants = {
    {MT_ROTX,  {-3.14, 3.14f,  3.14f, "rot_x"}},
    {MT_ROTY,  {-3.14, 3.14f,  2.74f, "rot_y"}},
    {MT_ROTZ,  {-3.14, 3.14f,  3.14f, "rot_z"}},
    {MT_TX,    {-1000.0f, 1000.f, -20.0f, "t_x"}},
    {MT_TY,    {-1000.0f, 1000.f,   5.0f, "t_y"}},
    {MT_TZ,    {-1000.0f, 1000.f,  10.0f, "t_z"}},
    {MT_SX,    { 0.1f, 10.0f,  1.0f, "s_x"}},
    {MT_SY,    { 0.1f, 10.0f,  1.0f, "s_y"}},
    {MT_SZ,    { 0.1f, 10.0f,  1.0f, "s_z"}},
};

static const std::map<ModelType, FloatField> ViewConstants = {
    {MT_ROTX,  {-3.14, 3.14f,  -0.253, "rot_x"}},
    {MT_ROTY,  {-3.14, 3.14f,  -0.250, "rot_y"}},
    {MT_ROTZ,  {-3.14, 3.14f,   0.0f,  "rot_z"}},
    {MT_TX,    {-1000.0f, 1000.f, -40.0f, "t_x"}},
    {MT_TY,    {-1000.0f, 1000.f, 100.0f, "t_y"}},
    {MT_TZ,    {-1000.0f, 1000.f, 160.0f, "t_z"}},
    {MT_SX,    { 0.1f, 10.0f,  1.0f, "s_x"}},
    {MT_SY,    { 0.1f, 10.0f,  1.0f, "s_y"}},
    {MT_SZ,    { 0.1f, 10.0f,  1.0f, "s_z"}},
};

const std::map<GluPerspectiveType, FloatField>& NdcConstants::GluPerspective()
{
    return GluPerspectiveConstants;
}

const std::map<FrustumType, FloatField>& NdcConstants::GlFrustum()
{
    return Frustum;
}

const std::map<ModelType, FloatField>& NdcConstants::Model()
{
    return ModelConstants;
}

const std::map<ModelType, FloatField>& NdcConstants::View()
{
    return ViewConstants;
}
