#ifndef RENDERER_H
#define RENDERER_H

#include "math/Matrix4.h"
#include "math/Vector4.h"
#include "Drawer.h"
#include "mdlv6.h"
#include "AbstractSettings.h"

#include <memory>
#include <vector>

class Renderer {
public:
     Renderer(void);
    ~Renderer(void);

    void Init();

    void setFillColor(uint8_t, uint8_t, uint8_t);
    void setWiredMode(void);
    void setFilledMode(void);
    void setDrawer(std::shared_ptr<Drawer>);
    void setSettings(std::shared_ptr<AbstractSettings> settings);

    std::shared_ptr<Drawer> getDrawer(void);

    void draw(void);

private:
    std::shared_ptr<Drawer> m_drawer;
    std::shared_ptr<AbstractSettings> m_settings;

    Matrix4 m_mvpMatrix;
    bool m_debugEnabled;
    RGB m_fillColor;

    mdlv6_t m_mdl;

    void updateViewMatrix(void);
    void updateModelMatrix(void);
    void updateProjectionMatrixFov(void);
    void updateProjectionMatrixFrustum(void);
    void computeMVP(void);

    void clipPolygonCoordToPlane(
            std::vector <Point>&, 
            std::vector <Point>&, 
            const int, 
            const float);
    bool clipPolygonCoord(std::vector <Point>&, int);
    bool clipPolygon(std::vector <Point>&);

    bool clipSegmentCoordToPlane(Vector4&, Vector4&, const int, const float);
    bool clipSegmentCoord(Vector4&, Vector4&, const int);
    bool clipSegment(Vector4&, Vector4&);

    void drawSegment(Point&, Point&);
    void drawTriangle(Point&, Point&, Point&);
    void drawPolygon(std::vector <Point>&);

    Vector4 transformVector4(const Vector4&);
    void    transformPointToViewport(Point&);
    void    transformAndDrawSegment(Vector4&, Vector4&);
    void    transformAndDrawTriangle(Vector4, Vector4, Vector4, Vector3*);
    void    transformModelVertex(Vector3&, const struct mdlv6_vertex_t*);

    void drawAxis(void);
    void drawGrid(void);

    void draw3DPyramid(void);
    void draw3DTriangle(void);
    void renderModelFrame(int);

    void readModel(void);
    int  readModel(const char* filename);
};

#endif
