#include "RGB.h"

RGB::RGB(void) {
    this->rgb[0] = 0;
    this->rgb[1] = 0;
    this->rgb[2] = 0;
}

RGB::RGB(uint8_t _r, uint8_t _g, uint8_t _b) {
    this->rgb[0] = _r;
    this->rgb[1] = _g;
    this->rgb[2] = _b;
}

RGB::RGB(uint8_t _rgb[3]) {
    this->rgb[0] = _rgb[0];
    this->rgb[1] = _rgb[1];
    this->rgb[2] = _rgb[2];
}

void RGB::set(uint8_t _r, uint8_t _g, uint8_t _b) {
    this->rgb[0] = _r;
    this->rgb[1] = _g;
    this->rgb[2] = _b;
}

void RGB::scale(float s) {
    this->rgb[0] = (uint8_t)(s * (float)this->rgb[0]);
    this->rgb[1] = (uint8_t)(s * (float)this->rgb[1]);
    this->rgb[2] = (uint8_t)(s * (float)this->rgb[2]);
}

uint8_t RGB::red(void) const {
    return this->rgb[0];
}

uint8_t RGB::green(void) const {
    return this->rgb[1];
}

uint8_t RGB::blue(void) const {
    return this->rgb[2];
}

uint8_t &RGB::operator[] (int index) {
    return this->rgb[index];
}
