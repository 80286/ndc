#ifndef RGB_H
#define RGB_H

#include <stdint.h>

class RGB {
private:
    uint8_t rgb[3];
public:
    RGB(void);
    RGB(uint8_t, uint8_t, uint8_t);
    RGB(uint8_t [3]);
    void set(uint8_t, uint8_t, uint8_t);
    void scale(float);
    uint8_t red(void) const;
    uint8_t green(void) const;
    uint8_t blue(void) const;
    uint8_t &operator[] (int);
};

#endif
