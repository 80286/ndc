#include "math/Vector3.h"

Vector3::Vector3(void) {
}

Vector3::Vector3(v3 v) {
    v3copy(this->array, v);
}

Vector3::Vector3(Vector4 v) {
    v3set(this->array, v[0], v[1], v[2]);
}

Vector3::Vector3(float x, float y, float z) {
    v3set(this->array, x, y, z);
}

void Vector3::set(float x, float y, float z) {
    v3set(this->array, x, y, z);
}

float Vector3::get(int index) const {
    return this->array[index];
}

Vector3::~Vector3(void) {
}

float Vector3::length(void) const {
    return v3length(this->array);
}

void Vector3::normalize(void) {
    v3normalize(this->array);
}

void Vector3::scale(float s) {
    v3scale(this->array, s);
}

void Vector3::print(const char *prefix) const {
    v3print(this->array, prefix);
}

float Vector3::dot(const Vector3 &other) const {
    return v3dot(this->array, other.array);
}

Vector3 Vector3::cross(const Vector3 &other) const {
    Vector3 result;

    v3cross(result.array, this->array, other.array);

    return result;
}

bool Vector3::isNDC(void) {
    return (v3isNDC(this->array) == 0);
}

void Vector3::viewportTransform(int x, int y, int width, int height) {
    v3viewportTransform(this->array, x, y, width, height);
}

void Vector3::depthRange(float nearVal, float farVal) {
    v3depthRange(this->array, nearVal, farVal);
}

void Vector3::setRayPoint(const Vector3 a, const Vector3 b, float t) {
    v3setRayPoint(this->array, a.array, b.array, t);
}

void Vector3::setRayPointDiff(const Vector3 a, const Vector3 b, float t) {
    v3setRayPointDiff(this->array, a.array, b.array, t);
}

void Vector3::operator+= (const Vector3 v) {
    v3add(this->array, v.array);
}

void Vector3::operator-= (const Vector3 v) {
    v3sub(this->array, v.array);
}

float &Vector3::operator[] (int index) {
    return this->array[index];
}

Vector3 operator+ (const Vector3 v, const Vector3 w) {
    Vector3 result;

    v3sum(result.array, v.array, w.array);
    return result;
}

Vector3 operator- (const Vector3 v, const Vector3 w) {
    Vector3 result;

    v3diff(result.array, v.array, w.array);
    return result;
}

Vector3 operator* (const Vector3 v, const Vector3 w) {
    Vector3 result;

    v3cross(result.array, v.array, w.array);
    return result;
}
