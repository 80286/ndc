#include "math/Matrix4.h"

Matrix4::Matrix4(void) {
    m4identity(this->array);
}

Matrix4::~Matrix4(void) {
}

void Matrix4::print(const char *prefix) const {
    m4print(this->array, prefix);
}

float Matrix4::get(int i, int j) const {
    return m4getElement(this->array, i, j);
}

void Matrix4::set(float x, int i, int j) {
    m4setElement(this->array, i, j, x);
}

void Matrix4::loadIdentity(void) {
    m4identity(this->array);
}

void Matrix4::setColumn(int i, Vector4 v) {
    m4setColumn(this->array, i, v.array);
}

void Matrix4::setRow(int i, Vector4 v) {
    m4setRow(this->array, i, v.array);
}

Vector4 Matrix4::getColumn(int i) const {
    Vector4 v;

    m4getColumn(v.array, this->array, i);

    return v;
}

Vector4 Matrix4::getRow(int i) const {
    Vector4 v;

    m4getRow(v.array, this->array, i);

    return v;
}

void Matrix4::transpose(void) {
    m4 result;

    m4transpose(result, this->array);
    m4copy(this->array, result);
}

void Matrix4::setFrustum(
        float left, 
        float right, 
        float bottom, 
        float top, 
        float near, 
        float far) {
    m4setFrustum(this->array, left, right, bottom, top, near, far);
}

void Matrix4::setSymmetricalFrustum(
        float near, 
        float far, 
        float width, 
        float height) {
    m4setSymmetricalFrustum(this->array, near, far, width, height);
}

void Matrix4::setPerspective(
        float fovy, 
        float aspect, 
        float zNear, 
        float zFar) {
    m4setPerspective(this->array, fovy, aspect, zNear, zFar);
}

void Matrix4::setXRotation(float angle) {
    m4setXRotation(this->array, angle);
}

void Matrix4::setYRotation(float angle) {
    m4setYRotation(this->array, angle);
}

void Matrix4::setZRotation(float angle) {
    m4setZRotation(this->array, angle);
}

void Matrix4::setXYZRotation(float angle, float x, float y, float z) {
    m4setXYZRotation(this->array, angle, x, y, z);
}

void Matrix4::setLookAt(Vector3 pos, 
        Vector3 target, 
        Vector3 up, 
        Vector3 translation) {
    m4setLookAt(this->array, 
            pos.array, target.array, up.array, translation.array);
}

void Matrix4::setTranslation(float tx, float ty, float tz) {
    m4setTranslation(this->array, tx, ty, tz);
}

void Matrix4::setScale(float sx, float sy, float sz) {
    m4setScale(this->array, sx, sy, sz);
}

void Matrix4::translate(float tx, float ty, float tz) {
    m4translate(this->array, tx, ty, tz);
}

void Matrix4::scaleXYZ(float sx, float sy, float sz) {
    m4scale(this->array, sx, sy, sz);
}

void Matrix4::scale(float s) {
    this->scaleXYZ(s, s, s);
}

void Matrix4::scaleX(float sx) {
    this->scaleXYZ(sx, 1.0f, 1.0f);
}

void Matrix4::scaleY(float sy) {
    this->scaleXYZ(1.0f, sy, 1.0f);
}

void Matrix4::scaleZ(float sz) {
    this->scaleXYZ(1.0f, 1.0f, sz);
}

void Matrix4::rotateX(float ang) {
    m4rotateX(this->array, ang);
}

void Matrix4::rotateY(float ang) {
    m4rotateY(this->array, ang);
}

void Matrix4::rotateZ(float ang) {
    m4rotateZ(this->array, ang);
}

void Matrix4::rotateXYZ(float ang, float x, float y, float z) {
    Matrix4 rotation;
    Matrix4 tmp;

    tmp = *this;
    rotation.setXYZRotation(ang, x, y, z);

    *this = rotation * tmp;
}

void Matrix4::operator= (const Matrix4 a) {
    m4copy(this->array, a.array);
}

Matrix4 operator* (const Matrix4 a, const Matrix4 b) {
    Matrix4 result;

    m4mulm4(result.array, a.array, b.array);
    return result;
}

Vector4 operator*(const Matrix4 a, const Vector4 v) {
    Vector4 result;

    m4mulv4(result.array, a.array, v.array);
    return result;
}
