#include "math/Edge.h"

Edge::Edge(Triangle *triangle, Point ymin, Point ymax, int yminIndex) {
    float xPrestep, yPrestep;
    float xDist;
    float yDist;

    this->ymin = ymin;
    this->ymax = ymax;

    /* X, Y distances from ymin to ymax vertex: */
    xDist = (float)(ymax.x - ymin.x);
    yDist = (float)(ymax.y - ymin.y);

    /* Increase of X from ymin to ymax: */
    this->x_step = (yDist != 0.0f) ? (xDist / yDist) : 0.0f;

    /* Compute initial values of x, y (Screen coords are still floats): */
    yPrestep = ceilf(ymin.y) - ymin.y;
    xPrestep = yPrestep * this->x_step;

    /* Compute initial values for ymin vertex: */
    this->x = ymin.x + yPrestep * this->x_step;

    this->depth = triangle->getDepth(yminIndex) +
                  triangle->getDepthStepY() * yPrestep + 
                  triangle->getDepthStepX() * xPrestep;
    this->depth_step = triangle->getDepthStepY() + 
                       triangle->getDepthStepX() * this->x_step;

    this->lightAmount = triangle->getLightAmount(yminIndex) +
                     triangle->getLightAmountStepY() * yPrestep + 
                     triangle->getLightAmountStepX() * xPrestep;
    this->lightAmount_step = triangle->getLightAmountStepY() + 
                          triangle->getLightAmountStepX() * this->x_step;
}

float Edge::getMinY(void) const {
    return this->ymin.y;
}

float Edge::getMaxY(void) const {
    return this->ymax.y;
}

float Edge::getX(void) const {
    return this->x;
}

float Edge::getDepth(void) const {
    return this->depth;
}

float Edge::getLightAmount(void) const {
    return this->lightAmount;
}

void Edge::step(void) {
    this->x += this->x_step;
    this->depth += this->depth_step;
    this->lightAmount += this->lightAmount_step;
}
