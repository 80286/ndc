#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "math/Point.h"
#include "math/common.h"

#include <algorithm>

class Triangle {
private:
    Point *vertices[3];
    float depth[3];
    float lightAmount[3];

    float depthStepX;
    float depthStepY;

    float lightAmountStepX;
    float lightAmountStepY;

public:
    Triangle(Point *, Point *, Point *);

    float calcStepX(float *, float);
    float calcStepY(float *, float);
    float calcMinX(void) const;
    float calcMaxX(void) const;

    Point *getYmin(void) const;
    Point *getYmid(void) const;
    Point *getYmax(void) const;

    float getDepth(int) const;
    float getDepthStepX(void) const;
    float getDepthStepY(void) const;

    float getLightAmount(int) const;
    float getLightAmountStepX(void) const;
    float getLightAmountStepY(void) const;
};

#endif
