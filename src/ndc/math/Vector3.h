#ifndef VECTOR3_H
#define VECTOR3_H

#include "math/c/v3.h"
#include "math/Vector4.h"

#define IN_RANGE(a, b, c) ((c) >= (a) ? ((c) <= (b)) : 0)

class Vector4;

class Vector3 {
public:
    v3 array;

     Vector3(void);
     Vector3(v3);
     Vector3(Vector4);
     Vector3(float, float, float);
    ~Vector3(void);

    void set(float, float, float);
    float get(int) const;
    float length(void) const;
    void normalize(void);
    void scale(float);
    void print(const char *prefix=NULL) const;
    float dot(const Vector3 &) const;
    Vector3 cross(const Vector3 &) const;

    bool isNDC(void);
    void viewportTransform(int, int, int, int);
    void depthRange(float, float);
    void setRayPoint(const Vector3, const Vector3, float);
    void setRayPointDiff(const Vector3, const Vector3, float);

    void operator+= (const Vector3);
    void operator-= (const Vector3);
    float &operator[] (int);
};

Vector3 operator+ (const Vector3, const Vector3);
Vector3 operator- (const Vector3, const Vector3);
Vector3 operator* (const Vector3, const Vector3);

#endif
