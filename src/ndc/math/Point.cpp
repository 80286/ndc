#include "math/Point.h"

Point::Point() {
    x = 0.0f;
    y = 0.0f;
}

Point::Point(const float newX, const float newY) {
    x = newX;
    y = newY;
}

Point::Point(Vector4 _pos, Vector3 _normal) {
    pos = _pos;
    normal = _normal;
}

void Point::set(const float newX, const float newY) {
    x = newX;
    y = newY;
}

void Point::set(const float xy[2]) {
    x = xy[0];
    y = xy[1];
}

void Point::setNormal(const Vector3& newNormal) {
    normal = newNormal;
}

void Point::print(const char *prefix) {
    printf("%s = [%g, %g];\n", prefix, x, y);
}

void Point::lerp(const Point &a, const Point &b, const float t) {
    pos.setRayPoint(a.pos, b.pos, t);
    normal.setRayPoint(a.normal, b.normal, t);
}

void Point::viewportTransform(const int width, const int height) {
    Vector3 pos_ndc = pos.divideByW();
    pos_ndc.viewportTransform(0, 0, width, height);

    set(pos_ndc[0], pos_ndc[1]);
}

const Point *Point::minY(const Point *other) {
    return y < other->y ? this : other;
}

const Point *Point::maxY(const Point *other) {
    return y > other->y ? this : other;
}
