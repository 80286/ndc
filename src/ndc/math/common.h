#ifndef COMMON_H
#define COMMON_H

template <class T>
inline T min(T a, T b) {
    return a < b ? a : b;
}

template <class T>
inline T max(T a, T b) {
    return a > b ? a : b;
}

template <class T>
T clamp(T value, T left, T right) {
    if (value < left) {
        return left;
    } else if (value > right) {
        return right;
    }

    return value;
}

template <class T>
bool inRange(T left, T right, T value) {
    return value >= left && value <= right;
}

#endif
