#ifndef MATRIX4_H
#define MATRIX4_H

#include "math/c/m4.h"
#include "math/Vector4.h"
#include "math/Vector3.h"

class Vector4;
class Vector3;

class Matrix4 {
public:
    m4 array;

     Matrix4(void);
    ~Matrix4(void);

    void print(const char *prefix=NULL) const;
    float get(int, int) const;
    Vector4 getColumn(int) const;
    Vector4 getRow(int) const;

    void transpose(void);
    void loadIdentity(void);

    void set(float, int, int);
    void setColumn(int, Vector4);
    void setRow(int, Vector4);
    void setFrustum(float, float, float, float, float, float);
    void setSymmetricalFrustum(float, float, float, float);
    void setPerspective(float, float, float, float);
    void setXRotation(float);
    void setYRotation(float);
    void setZRotation(float);
    void setXYZRotation(float, float, float, float);
    void setLookAt(Vector3, Vector3, Vector3, Vector3);
    void setTranslation(float, float, float);
    void setScale(float, float, float);

    void translate(float, float, float);
    void rotateX(float);
    void rotateY(float);
    void rotateZ(float);
    void rotateXYZ(float, float, float, float);
    void scaleXYZ(float, float, float);
    void scaleX(float);
    void scaleY(float);
    void scaleZ(float);
    void scale(float);

    void operator= (const Matrix4);
};

Matrix4 operator* (const Matrix4, const Matrix4);
Vector4 operator*(const Matrix4, const Vector4);

#endif
