#include "math/Vector4.h"

Vector4::Vector4(void) {
    v4zero(this->array);
}

Vector4::~Vector4(void) {
}

Vector4::Vector4(const Vector3 &v) {
    v4set(this->array, v.array[0], v.array[1], v.array[2], 1.0f);
}

Vector4::Vector4(const Vector3 &v, float _w) {
    v4set(this->array, v.array[0], v.array[1], v.array[2], _w);
}

Vector4::Vector4(float x, float y, float z, float w) {
    v4set(this->array, x, y, z, w);
}

void Vector4::set(float x, float y, float z, float w) {
    v4set(this->array, x, y, z, w);
}

void Vector4::print(const char *prefix) const {
    v4print(this->array, prefix);
}

Vector3 Vector4::divideByW() const {
    Vector3 v;

    v4divide(v.array, this->array);

    return v;
}

float Vector4::x(void) const {
    return this->array[0];
}

float Vector4::y(void) const {
    return this->array[1];
}

float Vector4::z(void) const {
    return this->array[2];
}

float Vector4::w(void) const {
    return this->array[3];
}

float Vector4::get(int index) const {
    return this->array[index];
}

void Vector4::zero(void) {
    v4zero(this->array);
}

void Vector4::normalize(void) {
    v4normalize(this->array);
}

float Vector4::dot(const Vector4 v) const {
    return v4dot(this->array, v.array);
}

void Vector4::scale(float s) {
    v4scale(this->array, s);
}

bool Vector4::isInsideFrustum(void) const {
    return (v4isInsideFrustum(this->array) == 0);
}

void Vector4::setRayPoint(const Vector4 &a, const Vector4 &b, float t) {
    v4setRayPoint(this->array, a.array, b.array, t);
}

void Vector4::setRayPointDiff(const Vector4 &a, const Vector4 &b, float t) {
    v4setRayPointDiff(this->array, a.array, b.array, t);
}

Vector3 Vector4::toVector3(void) const {
    Vector3 result(this->x(), this->y(), this->z());

    return result;
}

void Vector4::operator= (const Vector4 v) {
    v4copy(this->array, v.array);
}

Vector4 operator+ (const Vector4 v, const Vector4 w) {
    Vector4 result;

    v4sum(result.array, v.array, w.array);

    return result;
}

Vector4 operator- (const Vector4 v, const Vector4 w) {
    Vector4 result;

    v4diff(result.array, v.array, w.array);

    return result;
}

void Vector4::operator+= (const Vector4 v) {
    v4add(this->array, v.array);
}

void Vector4::operator-= (const Vector4 v) {
    v4sub(this->array, v.array);
}

float &Vector4::operator[] (int index) {
    return this->array[index];
}
