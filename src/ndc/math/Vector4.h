#ifndef VECTOR4_H
#define VECTOR4_H

#include "math/c/v4.h"
#include "math/Vector3.h"
#include "math/Matrix4.h"

class Matrix4;

class Vector4 {
public:
    v4 array;

     Vector4(void);
     Vector4(const Vector3 &);
     Vector4(const Vector3 &, float);
     Vector4(float, float, float, float);
    ~Vector4(void);

    void set(float, float, float, float);
    void print(const char *prefix=NULL) const;
    const float *getArray(void) const;
    Vector3 divideByW(void) const;
    float x(void) const;
    float y(void) const;
    float z(void) const;
    float w(void) const;
    float get(int) const;
    void zero(void);
    void normalize(void);
    float dot(const Vector4) const;
    bool isInsideFrustum(void) const;
    void setRayPoint(const Vector4 &, const Vector4 &, float);
    void setRayPointDiff(const Vector4 &, const Vector4 &, float);
    void scale(float);
    Vector3 toVector3(void) const;

    void operator= (const Vector4);
    void operator+= (const Vector4);
    void operator-= (const Vector4);
    float &operator[] (int);
};

Vector4 operator+ (const Vector4, const Vector4);
Vector4 operator- (const Vector4, const Vector4);

#endif
