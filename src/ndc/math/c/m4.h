#ifndef M4_H
#define M4_H

#include "math/c/v4.h"

#if __cplusplus
extern "C" {
#endif

typedef float m4[16];

void m4print(const m4, const char *);
float m4getElement(const m4, int, int);
void m4setElement(m4, int, int, float);
void m4init(m4);
void m4identity(m4);
void m4setColumn(m4, int, const v4);
void m4setRow(m4, int, const v4);
void m4getColumn(v4, const m4, int);
void m4getRow(v4, const m4, int);
void m4copy(m4, const m4);
void m4transpose(m4, const m4);
void m4mulv4(v4, const m4, const v4);
void m4mulm4(m4, const m4, const m4);
void m4setFrustum(m4, float, float, float, float, float, float);
void m4setSymmetricalFrustum(m4, float, float, float, float);
void m4setPerspective(m4, float, float, float, float);
void m4setLookAt(m4, v3, v3, v3, v3);
void m4setXRotation(m4, float);
void m4setYRotation(m4, float);
void m4setZRotation(m4, float);
void m4setXYZRotation(m4, float, float, float, float);
void m4setTranslation(m4, float, float, float);
void m4setScale(m4, float, float, float);

void m4translate(m4, float, float, float);
void m4scale(m4, float, float, float);
void m4rotateX(m4, float);
void m4rotateY(m4, float);
void m4rotateZ(m4, float);

#if __cplusplus
}
#endif

#endif
