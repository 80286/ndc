#ifndef V4_H
#define V4_H

#include "math/c/v3.h"

#if __cplusplus
extern "C" {
#endif

typedef float v4[4];

void v4print(const v4, const char *);
void v4zero(v4);
void v4set(v4, float, float, float, float);
void v4copy(v4, const v4);
void v4divide(v3, const v4);
void v4scale(v4, float);
float v4dot(const v4, const v4);
void v4add(v4, const v4);
void v4sub(v4, const v4);
void v4diff(v4, const v4, const v4);
void v4sum(v4, const v4, const v4);
void v4normalize(v4);
int  v4isInsideFrustum(const v4);
void v4setRayPoint(v4, const v4, const v4, float);
void v4setRayPointDiff(v4, const v4, const v4, float);

#if __cplusplus
}
#endif

#endif
