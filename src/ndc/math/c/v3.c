#include "math/c/v3.h"

/*
 * C implementation of basic operations
 * on 3-dimensional vectors.
 */

/**************************************************
 * v3set();
 * 
 * Sets new float coordinates of vector $v.
 *************************************************/
void v3set(v3 v, float x, float y, float z) {
    v[0] = x;
    v[1] = y;
    v[2] = z;
}

/**************************************************
 * v3setArray();
 * 
 * Copy array values to vector $v.
 *************************************************/
void v3setArray(v3 v, float *w) {
    v[0] = w[0];
    v[1] = w[1];
    v[2] = w[2];
}

/**************************************************
 * v3copy();
 * 
 * Copies values from vector $src to $dst.
 *************************************************/
void v3copy(v3 dst, const v3 src) {
    dst[0] = src[0];
    dst[1] = src[1];
    dst[2] = src[2];
}

/**************************************************
 * v3length();
 *
 * Returns magnitude of vector $v.
 *************************************************/
float v3length(const v3 v) {
    return sqrtf((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]));
}

/**************************************************
 * v3normalize();
 *
 * Normalizes vector $v.
 *************************************************/
void v3normalize(v3 v) {
    float vlen;

    vlen = v3length(v);

    if (vlen == 0.0f) {
        return;
    }

    v[0] /= vlen;
    v[1] /= vlen;
    v[2] /= vlen;
}

/**************************************************
 * v3scale();
 * 
 * Scales vector $v by factor $scale.
 *************************************************/
void v3scale(v3 v, float scale) {
    v[0] *= scale;
    v[1] *= scale;
    v[2] *= scale;
}

/**************************************************
 * v3add();
 * 
 * Adds vector $v to $result.
 *************************************************/
void v3add(v3 result, const v3 v) {
    result[0] += v[0];
    result[1] += v[1];
    result[2] += v[2];
}

/**************************************************
 * v3sub();
 * 
 * Subtracts vector $v from vector $result.
 *************************************************/
void v3sub(v3 result, const v3 v) {
    result[0] -= v[0];
    result[1] -= v[1];
    result[2] -= v[2];
}

/**************************************************
 * v3sum();
 * 
 * Returns ($va - $vb) to vector $result.
 *************************************************/
void v3sum(v3 result, const v3 va, const v3 vb) {
    result[0] = va[0] + vb[0];
    result[1] = va[1] + vb[1];
    result[2] = va[2] + vb[2];
}

/**************************************************
 * v3diff();
 * 
 * Returns ($va - $vb) to vector $result.
 *************************************************/
void v3diff(v3 result, const v3 va, const v3 vb) {
    result[0] = va[0] - vb[0];
    result[1] = va[1] - vb[1];
    result[2] = va[2] - vb[2];
}

/**************************************************
 * v3cross();
 * 
 * Returns cross product of vectors $va and $b
 * to $result.
 *************************************************/
void v3cross(v3 result, const v3 va, const v3 vb) {
    result[0] = va[1] * vb[2] - va[2] * vb[1];
    result[1] = va[2] * vb[0] - va[0] * vb[2];
    result[2] = va[0] * vb[1] - va[1] * vb[0];
}

/**************************************************
 * v3dot();
 * 
 * Returns dot product of vectors $va and $vb.
 *************************************************/
float v3dot(const v3 va, const v3 vb) {
    return (va[0] * vb[0]) + (va[1] * vb[1]) + (va[2] * vb[2]);
}

/**************************************************
 * v3print();
 * 
 * Prints formatted information about vector $v.
 *************************************************/
void v3print(const v3 v, const char *prefix) {
    if (prefix == NULL) {
        printf("%p:[%g, %g, %g];\n", v, v[0], v[1], v[2]);
    } else {
        printf("%s:[%g, %g, %g];\n", prefix, v[0], v[1], v[2]);
    }
}

/**************************************************
 * v3comb();
 * 
 * Returns (a1 * w1 + a2 * w2 + a3 * w3) to 
 * vector $result.
 *************************************************/
void v3comb(v3 result, float a1, float a2, float a3, const v3 w1, const v3 w2, const v3 w3) {
    result[0] = a1 * w1[0] + a2 * w2[0] + a3 * w3[0];
    result[1] = a1 * w1[1] + a2 * w2[1] + a3 * w3[1];
    result[2] = a1 * w1[2] + a2 * w2[2] + a3 * w3[2];
}

/**************************************************
 * v3viewportTransform();
 * 
 * Transforms first two Normalized Device Coordinates
 * of vector $r * to Viewport.
 *************************************************/
void v3viewportTransform(v3 r, int x, int y, int width, int height) {
    r[0] = ((float)(width  / 2) * r[0]) + (float)(x + width  / 2);
    r[1] = ((float)(height / 2) * r[1]) + (float)(y + height / 2);
}

/**************************************************
 * v3depthRange();
 * 
 * Transforms third Normalized Device Coordinate to range
 * (nearVal, farVal).
 *************************************************/
void v3depthRange(v3 r, float nearVal, float farVal) {
    if (nearVal >= farVal) {
        printf("v3depthRange(); nearVal=%g >= farVal=%g\n", nearVal, farVal);
        return;
    }
    r[2] = ((farVal - nearVal) * 0.5f * r[2]) + (farVal + nearVal) * 0.5f;
}

int v3isNDC(const v3 v) {
    if (v[0] >= -1.0f && v[0] <= 1.0f &&
        v[1] >= -1.0f && v[1] <= 1.0f &&
        v[2] >= -1.0f && v[2] <= 1.0f) {
        return 0;
    }
    return 1;
}

/**************************************************
 * v3setRayPoint();
 * 
 * Assigns (a + t * (b - a)) to vector r.
 *************************************************/
void v3setRayPoint(v3 r, const v3 a, const v3 b, float t) {
    r[0] = a[0] + t * (b[0] - a[0]);
    r[1] = a[1] + t * (b[1] - a[1]);
    r[2] = a[2] + t * (b[2] - a[2]);
}

/**************************************************
 * v3setRayPointDiff();
 * 
 * Assigns (a + t * b) to vector r.
 *************************************************/
void v3setRayPointDiff(v3 r, const v3 a, const v3 b, float t) {
    r[0] = a[0] + t * b[0];
    r[1] = a[1] + t * b[1];
    r[2] = a[2] + t * b[2];
}
