#ifndef V3_H
#define V3_H

#include <math.h>
#include <stdio.h>

#if __cplusplus
extern "C" {
#endif

typedef float v3[3];

void  v3set         (v3 v, float, float, float);
void  v3setArray    (v3 v, float *);
void  v3copy        (v3, const v3);
float v3length      (const v3 );
void  v3normalize   (v3);
void  v3scale       (v3, float);
void  v3add         (v3, const v3);
void  v3sub         (v3, const v3);
void  v3sum         (v3, const v3, const v3);
void  v3diff        (v3, const v3, const v3);
void  v3cross       (v3, const v3, const v3);
float v3dot         (const v3, const v3);
void  v3print       (const v3, const char *);
void  v3comb        (v3 , float, float, float, const v3, const v3, const v3);
void  v3viewportTransform(v3, int, int, int, int);
void  v3depthRange(v3, float, float);
int   v3isNDC(const v3);
void  v3setRayPoint(v3, const v3, const v3, float);
void  v3setRayPointDiff(v3, const v3, const v3, float);

#if __cplusplus
}
#endif

#endif
