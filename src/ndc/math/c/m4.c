#include "math/c/m4.h"

/*
 * C implementation of basic operations 
 * on 4x4 matrices, stored in row-major
 * order.
 *
 * [row-major order]:
 * 0  1  2  3
 * 4  5  6  7
 * 8  9  10 11
 * 12 13 14 15
 *
 * column-major order:
 * 0  4  8  12
 * 1  5  9  13
 * 2  6  10 14
 * 3  7  11 15
 */

/**************************************************
 * m4print();
 * 
 * Print matrix m with additional description (optional).
 *************************************************/
void m4print(const m4 m, const char *prefix) {
    int i, j;
    float e;

    if (prefix == NULL) {
        printf("%p:matrix(\n", (void *)m);
    } else {
        printf("%s:matrix(\n", prefix);
    }
    for (i = 0; i < 4; ++i) {
        printf("[ ");
        for (j = 0; j < 4; ++j) {
            e = m4getElement(m, i, j);
            printf("%s%5.5f%s ", 
                    (e >= 0.0f) ? " " : "",
                    e,
                    (j != 3) ? "," : "");
        }
        printf("]%s\n", (i != 3) ? "," : "");
    }
    printf(");\n");
}

/**************************************************
 * m4getElement();
 * 
 * Retrieves matrix element from i-th row and j-th column.
 *************************************************/
float m4getElement(const m4 m, int i, int j) {
    return m[4 * i + j];
}

/**************************************************
 * m4setElement();
 * 
 * Replaces m element from i-th row
 * and j-th column with x element.
 *************************************************/
void m4setElement(m4 m, int i, int j, float x) {
    m[4 * i + j] = x;
}

/**************************************************
 * m4init();
 * 
 * m[i, j] := 0
 * i, j = 0...n-1
 *************************************************/
void m4init(m4 m) {
    int i;

    for(i = 0; i < 16; i++) {
        m[i] = 0.0f;
    }
}

/**************************************************
 * m4identity(m);
 *
 * Replaces matrix with identity matrix.
 * 
 * m[i, j] := { 0, i != j
 *            { 1, i == j
 * i, j = 0, ... , n - 1
 *************************************************/
void m4identity(m4 m) {
    m[1 ] = m[2 ] = m[3 ] = 0.0f;
    m[4 ] = m[6 ] = m[7 ] = 0.0f;
    m[8 ] = m[9 ] = m[11] = 0.0f;
    m[12] = m[13] = m[14] = 0.0f;
    m[0 ] = m[5 ] = m[10] = m[15] = 1.0f;
}

/**************************************************
 * m4setColumn(r, i, v);
 *
 * Replaces i-th column of matrix r with vector v.
 *
 * r = c1 c2 c3 c4
 * ci := v
 *************************************************/
void m4setColumn(m4 r, int i, const v4 v) {
    r[4 * 0 + i] = v[0];
    r[4 * 1 + i] = v[1];
    r[4 * 2 + i] = v[2];
    r[4 * 3 + i] = v[3];
}

/**************************************************
 * m4setRow(r, i, v);
 *
 * Replaces i-th row of matrix r with vector v.
 *
 * r = r1 r2 r3 r4
 * ri := v
 *************************************************/
void m4setRow(m4 r, int i, const v4 v) {
    r[4 * i + 0] = v[0];
    r[4 * i + 1] = v[1];
    r[4 * i + 2] = v[2];
    r[4 * i + 3] = v[3];
}

/**************************************************
 * m4getColumn(m, i, v);
 *
 * Retrieves i-th column from matrix m to vector r.
 *
 * m = c1 c2 c3 c4
 * r := c_i
 *************************************************/
void m4getColumn(v4 r, const m4 m, int i) {
    r[0] = m[4 * 0 + i];
    r[1] = m[4 * 1 + i];
    r[2] = m[4 * 2 + i];
    r[3] = m[4 * 3 + i];
}

/**************************************************
 * m4getRow(m, i, v);
 *
 * Retrieves i-th row from matrix to vector r.
 *
 * m = r1 r2 r3 r4
 * r := r_i
 *************************************************/
void m4getRow(v4 r, const m4 m, int i) {
    r[0] = m[4 * i + 0];
    r[1] = m[4 * i + 1];
    r[2] = m[4 * i + 2];
    r[3] = m[4 * i + 3];
}

/**************************************************
 * m4copy(r, s);
 *
 * Copies matrix s to matrix r.
 *
 * r[i, j] := s[i, j]
 * i, j = 0...n-1
 *************************************************/
void m4copy(m4 r, const m4 s) {
    int i;

    for (i = 0; i < 16; ++i) {
        r[i] = s[i];
    }
}

/**************************************************
 * m4transpose(r, m);
 * 
 * Transposes matrix m to matrix r.
 *************************************************/
void m4transpose(m4 r, const m4 m) {
    r[0]  = m[0 ];
    r[1]  = m[4 ];
    r[2]  = m[8 ];
    r[3]  = m[12];
         
    r[4]  = m[1 ];
    r[5]  = m[5 ];
    r[6]  = m[9 ];
    r[7]  = m[13];

    r[8]  = m[2 ];
    r[9]  = m[6 ];
    r[10] = m[10];
    r[11] = m[14];

    r[12] = m[3 ];
    r[13] = m[7 ];
    r[14] = m[11];
    r[15] = m[15];
}

/**************************************************
 * m4mulv4(r, m, v)
 *
 * Multiplies matrix m by vector v to vector r.
 *
 * r := m * v^T
 *************************************************/
void m4mulv4(v4 r, const m4 m, const v4 v) {
    r[0] = m[0 ] * v[0] + m[1 ] * v[1] + m[2 ] * v[2] + m[3 ] * v[3];
    r[1] = m[4 ] * v[0] + m[5 ] * v[1] + m[6 ] * v[2] + m[7 ] * v[3];
    r[2] = m[8 ] * v[0] + m[9 ] * v[1] + m[10] * v[2] + m[11] * v[3];
    r[3] = m[12] * v[0] + m[13] * v[1] + m[14] * v[2] + m[15] * v[3];
}

/**************************************************
 * m4mulm4(r, a, b)
 *
 * Multiplies matrix a by matrix b, to matrix r.
 *
 * r := a * b
 *************************************************/
void m4mulm4(m4 r, const m4 a, const m4 b) {
    /* int k; */

    /* for (k = 0; k < 16; ++k) { */
    /*     r[k] = a[4 * (k / 4) + k] * b[4 * k + k % 4]; */
    /* } */

    v4 column, row;
    int i, j;

    for (i = 0; i < 4; ++i) {
        m4getRow(row, a, i);
        for (j = 0; j < 4; ++j) {
            m4getColumn(column, b, j);

            /* r[4 * j + i] = v4dot(row, column); */
            m4setElement(r, i, j, v4dot(row, column));
        }
    }
}

/**************************************************
 * m4SetFrustum();
 * 
 * Defines viewing frustum in matrix r.
 *************************************************/
void m4setFrustum(m4 r, 
        float left, float right, 
        float bottom, float top, 
        float near, float far) {
    r[0 ] = (2.0f * near / (right - left));
    r[1 ] = 0.0f;
    r[2 ] = (right + left) / (right - left);
    r[3 ] = 0.0f;
         
    r[4 ] = 0.0f;
    r[5 ] = (2.0f * near) / (top - bottom);
    r[6 ] = (top + bottom) / (top - bottom);
    r[7 ] = 0.0f;
         
    r[8 ] = 0.0f;
    r[9 ] = 0.0f;
    r[10] = -(far + near) / (far - near);
    r[11] = (-2.0f * far * near) / (far - near);

    r[12] =  0.0f;
    r[13] =  0.0f;
    r[14] = -1.0f;
    r[15] =  0.0f;
}

/**************************************************
 * m4setSymmetricalFrustum();
 * 
 * 
 * m4setFrustum() for (left = -right) && (top = -bottom).
 *************************************************/
void m4setSymmetricalFrustum(
        m4 r, 
        float near, 
        float far, 
        float width, 
        float height) {
    r[0 ] = near / width;
    r[1 ] = r[2] = r[3] = 0.0f; 

    r[4 ] = r[6] = r[7] = 0.0f;
    r[5 ] = near / height;
         
    r[8 ] = r[9] = r[12] =  0.0f;
    r[10] = -(far + near) / (far - near);
    r[11] = (-2.0f * far * near) / (far - near);

    r[12] = r[13] = r[15] = 0.0f;
    r[14] = -1.0f;
}

/**************************************************
 * m4setPerspective();
 * 
 * Defines viewing frustum for matrix m.
 * https://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml
 *************************************************/
void m4setPerspective(
        m4 r, 
        float fovy, 
        float aspect_ratio, 
        float zNear, 
        float zFar) {
    float height, width;
    float deg2rad;
    
    deg2rad = M_PI / 180.0f;

    height = zFar * tanf((fovy * 0.5f) * deg2rad);
    width = height * aspect_ratio;

    m4setSymmetricalFrustum(r, zNear, zFar, width, height);
}

/**************************************************
 * m4setLookAt();
 * 
 * Sets camera properties factors in matrix m.
 * https://www.opengl.org/sdk/docs/man2/xhtml/gluLookAt.xml
 *************************************************/
void m4setLookAt(m4 r, v3 position, v3 target, v3 up, v3 translation) {
    v3 forward;
    v3 left;
    
    /* forward := target - position */
    v3diff(forward, target, position);
    v3normalize(forward);

    /* left := up x forward */
    v3cross(left, up, forward);
    v3normalize(left);

    /* up := forward x left */
    v3cross(up, forward, left);
    v3normalize(up);

    /* new orthonormal base: (forward, left, up) */

    r[0 ] = left[0];
    r[4 ] = left[1];
    r[8 ] = left[2];

    r[1 ] = up[0];
    r[5 ] = up[1];
    r[9 ] = up[2];

    r[2 ] = forward[0];
    r[6 ] = forward[1];
    r[10] = forward[2];

    r[3 ] = -translation[0];
    r[7 ] = -translation[1];
    r[11] = -translation[2];

    r[12] = 0.0f;
    r[13] = 0.0f;
    r[14] = 0.0f;
    r[15] = 1.0f;
}

/**************************************************
 * m4setXRotation();
 * 
 * Sets rotation around X axis factors in matrix m.
 *************************************************/
void m4setXRotation(m4 m, float r) {
    float sr, cr;

    sr = sinf(r);
    cr = cosf(r);
    m[0 ] = 1.0f; m[1 ] = 0.0f; m[2 ] = 0.0f; m[3 ] = 0.0f;
    m[4 ] = 0.0f; m[5 ] =   cr; m[6 ] =  -sr; m[7 ] = 0.0f;
    m[8 ] = 0.0f; m[9 ] =   sr; m[10] =   cr; m[11] = 0.0f;
    m[12] = 0.0f; m[13] = 0.0f; m[14] = 0.0f; m[15] = 1.0f;
}

/**************************************************
 * m4setYRotation();
 * 
 * Sets rotation around Y axis factors in matrix m.
 *************************************************/
void m4setYRotation(m4 m, float r) {
    float sr, cr;

    sr = sinf(r);
    cr = cosf(r);
    m[0 ] =   cr; m[1 ] = 0.0f; m[2 ] =   sr; m[3 ] = 0.0f;
    m[4 ] = 0.0f; m[5 ] = 1.0f; m[6 ] = 0.0f; m[7 ] = 0.0f;
    m[8 ] =  -sr; m[9 ] = 0.0f; m[10] =   cr; m[11] = 0.0f;
    m[12] = 0.0f; m[13] = 0.0f; m[14] = 0.0f; m[15] = 1.0f;
}

/**************************************************
 * m4setZRotation();
 * 
 * Sets rotation around Z axis factors in matrix m.
 *************************************************/
void m4setZRotation(m4 m, float r) {
    float sr, cr;

    sr = sinf(r);
    cr = cosf(r);
    m[0 ] =   cr; m[1 ] =  -sr; m[2 ] = 0.0f; m[3 ] = 0.0f;
    m[4 ] =   sr; m[5 ] =   cr; m[6 ] = 0.0f; m[7 ] = 0.0f;
    m[8 ] = 0.0f; m[9 ] = 0.0f; m[10] = 1.0f; m[11] = 0.0f;
    m[12] = 0.0f; m[13] = 0.0f; m[14] = 0.0f; m[15] = 1.0f;
}

/**************************************************
 * m4setXYZRotation();
 * 
 * Pre-multiplies matrix m by rotation matrix.
 * (Rotation around vector [x, y, z]).
 * [OpenGL uses post-multiplication for glRotatef()]
 *
 * https://www.opengl.org/sdk/docs/man2/xhtml/glRotate.xml
 *************************************************/
void m4setXYZRotation(m4 m, float ang, float x, float y, float z) {
    float c, s;

    c = sinf(ang);
    s = cosf(ang);

    m[0 ] = x * x * (1.0f - c) + c;
    m[1 ] = x * y * (1.0f - c) - (z * s);
    m[2 ] = x * z * (1.0f - c) + (y * s);
    m[3 ] = 0.0f;

    m[4 ] = x * y * (1.0f - c) + (z * s);
    m[5 ] = y * y * (1.0f - c) + c;
    m[6 ] = y * z * (1.0f - c) - (x * s);
    m[7 ] = 0.0f;

    m[8 ] = x * z * (1.0f - c) - (y * s);
    m[9 ] = y * z * (1.0f - c) + (x * s);
    m[10] = z * z * (1.0f - c) + c;
    m[11] = 0.0f;
    
    m[12] = m[13] = m[14] = 0.0f;
    m[15] = 1.0f;
}

/**************************************************
 * m4setTranslation();
 * 
 * Sets translation factors in matrix m.
 *************************************************/
void m4setTranslation(m4 m, float tx, float ty, float tz) {
    m[3 ] = tx;
    m[7 ] = ty;
    m[11] = tz;
}

/**************************************************
 * m4setScale();
 * 
 * Sets scale factors in matrix m.
 *************************************************/
void m4setScale(m4 m, float sx, float sy, float sz) {
    m[0 ] = sx;
    m[5 ] = sy;
    m[10] = sz;
}

/**************************************************
 * m4translate();
 * 
 * Pre-multiplies matrix m by translation matrix
 * (tx, ty, tz).
 * [OpenGL uses post-multiplication for glTranslatef()]
 *
 * Alt version:
 *
 * m4 translationMatrix;
 * m4 resultMatrix;
 * m4identity(translationMatrix);
 * m4setTranslation(translation, tx, ty, tz);
 * m4mulm4(resultMatrix, translationMatrix, m);
 *
 * https://www.opengl.org/sdk/docs/man2/xhtml/glTranslate.xml
 *************************************************/
void m4translate(m4 m, float tx, float ty, float tz) {
    m[0 ] += m[12] * tx;
    m[1 ] += m[13] * tx;
    m[2 ] += m[14] * tx;
    m[3 ] += m[15] * tx;

    m[4 ] += m[12] * ty;
    m[5 ] += m[13] * ty;
    m[6 ] += m[14] * ty;
    m[7 ] += m[15] * ty;

    m[8 ] += m[12] * tz;
    m[9 ] += m[13] * tz;
    m[10] += m[14] * tz;
    m[11] += m[15] * tz;
}

/**************************************************
 * m4scale();
 * 
 * Pre-multiplies matrix m by scale matrix
 * (sx, sy, sz).
 * [OpenGL uses post-multiplication for glScalef()]
 *
 * Alt version:
 *
 * m4 scaleMatrix;
 * m4 resultMatrix;
 * m4identity(scaleMatrix);
 * m4setScale(scaleMatrix, sx, sy, sz);
 * m4mulm4(resultMatrix, scaleMatrix, m);
 * 
 * https://www.opengl.org/sdk/docs/man2/xhtml/glScale.xml
 *************************************************/
void m4scale(m4 m, float sx, float sy, float sz) {
    m[0 ] *= sx;
    m[1 ] *= sx;
    m[2 ] *= sx;
    m[3 ] *= sx;

    m[4 ] *= sy;
    m[5 ] *= sy;
    m[6 ] *= sy;
    m[7 ] *= sy;

    m[8 ] *= sz;
    m[9 ] *= sz;
    m[10] *= sz;
    m[11] *= sz;
}

/**************************************************
 * m4rotateX();
 * 
 * Pre-multiplies matrix m by rotation matrix
 * (Rotate around X axis).
 *
 * Alt version:
 *
 * m4 rotationMatrix;
 * m4 resultMatrix;
 * m4identity(rotationMatrix);
 * m4setXRotation(rotationMatrix, ang);
 * m4mulm4(resultMatrix, rotationMatrix, m);
 *************************************************/
void m4rotateX(m4 m, float ang) {
    float a4, a5, a6, a7, a8, a9, a10, a11;
    float s, c;

    a4  = m[4 ];
    a5  = m[5 ];
    a6  = m[6 ];
    a7  = m[7 ];
       
    a8  = m[8 ];
    a9  = m[9 ];
    a10 = m[10];
    a11 = m[11];

    s = sinf(ang);
    c = cosf(ang);

    m[4 ] = a4 * c - a8  * s;
    m[5 ] = a5 * c - a9  * s;
    m[6 ] = a6 * c - a10 * s;
    m[7 ] = a7 * c - a11 * s;

    m[8 ] = a4 * s + a8  * c;
    m[9 ] = a5 * s + a9  * c;
    m[10] = a6 * s + a10 * c;
    m[11] = a7 * s + a11 * c;
}

/**************************************************
 * m4rotateY();
 * 
 * Pre-multiplies matrix m by rotation matrix
 * (Rotates around Y axis).
 * 
 * Alt version:
 * 
 * m4 rotationMatrix;
 * m4 resultMatrix;
 * m4identity(rotationMatrix);
 * m4setYRotation(rotationMatrix, ang);
 * m4mulm4(resultMatrix, rotationMatrix, m);
 *************************************************/
void m4rotateY(m4 m, float ang) {
    float a0, a1, a2, a3, a8, a9, a10, a11;
    float s, c;

    a0 = m[0];
    a1 = m[1];
    a2 = m[2];
    a3 = m[3];

    a8 = m[8];
    a9 = m[9];
    a10 = m[10];
    a11 = m[11];

    s = sinf(ang);
    c = cosf(ang);

    m[0 ] = a8  * s + a0  * c;
    m[1 ] = a9  * s + a1  * c;
    m[2 ] = a10 * s + a2  * c;
    m[3 ] = a11 * s + a3  * c;

    m[8 ] = a8  * c - a0  * s;
    m[9 ] = a9  * c - a1  * s;
    m[10] = a10 * c - a2  * s;
    m[11] = a11 * c - a3  * s;
}

/**************************************************
 * m4rotateZ();
 * 
 * Pre-multiplies matrix m by rotation matrix
 * (Rotates around Z axis).
 * 
 * Alt version:
 * 
 * m4 rotationMatrix;
 * m4 resultMatrix;
 * m4identity(rotationMatrix);
 * m4setZRotation(rotationMatrix, ang);
 * m4mulm4(resultMatrix, rotationMatrix, m);
 *************************************************/
void m4rotateZ(m4 m, float ang) {
    float a0, a1, a2, a3, a4, a5, a6, a7;
    float s, c;

    a0 = m[0];
    a1 = m[1];
    a2 = m[2];
    a3 = m[3];

    a4 = m[4];
    a5 = m[5];
    a6 = m[6];
    a7 = m[7];

    s = sinf(ang);
    c = cosf(ang);

    m[0] = a0 * c - a4 * s;
    m[1] = a1 * c - a5 * s;
    m[2] = a2 * c - a6 * s;
    m[3] = a3 * c - a7 * s;

    m[4] = a0 * s + a4 * c;
    m[5] = a1 * s + a5 * c;
    m[6] = a2 * s + a6 * c;
    m[7] = a3 * s + a7 * c;
}
