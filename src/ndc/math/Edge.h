#ifndef EDGE_H
#define EDGE_H

#include "math/Point.h"
#include "math/Triangle.h"

class Edge {
private:
    Point ymin;
    Point ymax;

    float x;
    float x_step;
    float depth;
    float depth_step;
    float lightAmount;
    float lightAmount_step;

public:
    Edge(Triangle *, Point, Point, int);

    float getMinY(void) const;
    float getMaxY(void) const;
    float getX(void) const;
    float getDepth(void) const;
    float getLightAmount(void) const;

    void step(void);
};


#endif
