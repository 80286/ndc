#ifndef POINT_H
#define POINT_H

#include <stdio.h>
#include "math/Vector4.h"
#include "math/Vector3.h"

class Point {
public:
    Point();
    Point(Vector4, Vector3);
    Point(const float, const float);

    float x;
    float y;
    Vector4 pos;
    Vector3 normal;

    void print(const char *);
    void set(const float, const float);
    void set(const float, const float, const float);
    void set(const float [2]);
    void setNormal(const Vector3&);
    void viewportTransform(const int width, const int height);
    void lerp(const Point &, const Point &, const float);
    const Point *minY(const Point *);
    const Point *maxY(const Point *);
};

#endif
