#include "math/Triangle.h"

Triangle::Triangle(Point *ymin, Point *ymid, Point *ymax) {
    float constStepDenominator;

    this->vertices[0] = ymin;
    this->vertices[1] = ymid;
    this->vertices[2] = ymax;

    this->depth[0] = ymin->pos.z();
    this->depth[1] = ymid->pos.z();
    this->depth[2] = ymax->pos.z();

    /* 
     * lightdir = [0, 0, 1]
     * lightAmount[i] = dot(normal, lightdir) == normal[2] */
    this->lightAmount[0] = ymin->normal[2];
    this->lightAmount[1] = ymid->normal[2];
    this->lightAmount[2] = ymax->normal[2];

    constStepDenominator = 
          (ymid->x - ymin->x) * (ymax->y - ymin->y) -
          (ymid->y - ymin->y) * (ymax->x - ymin->x);
    constStepDenominator = 1.0f / constStepDenominator;

    this->depthStepX = this->calcStepX(this->depth, constStepDenominator);
    this->depthStepY = this->calcStepY(this->depth, constStepDenominator);
    
    this->lightAmountStepX = this->calcStepX(this->lightAmount, constStepDenominator);
    this->lightAmountStepY = this->calcStepY(this->lightAmount, constStepDenominator);
}

/**************************************************
 * Triangle::calcStepX();
 * 
 * Calculate increase of value per pixel inside triangle 
 * (along horizontal screen axis):
 *
 *  dC     (c1 - c0) * (y2 - y0) - (y1 - y0) * (c2 - c0)
 * ----  = ---------------------------------------------
 *  dx     (x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0)
 *
 * (ymin, ymid, ymax) == ((x0, y0), (x1, y1), (x2, y2))
 *************************************************/
float Triangle::calcStepX(
        float *vertexValues, 
        float iDenominator) {
    float result;

    result = (vertexValues[1]      - vertexValues[0]) 
           * (this->vertices[2]->y - this->vertices[0]->y) -
             (this->vertices[1]->y - this->vertices[0]->y) 
           * (vertexValues[2]      - vertexValues[0]);

    return result * iDenominator;
}

/**************************************************
 * Triangle::calcStepY();
 * 
 * Calculate increase of value per pixel inside triangle 
 * (along vertical screen axis):
 *
 *  dC     (c2 - c0) * (x1 - x0) - (x2 - x0) * (c1 - c0)
 * ----  = ---------------------------------------------
 *  dy     (x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0)
 *
 * (ymin, ymid, ymax) == ((x0, y0), (x1, y1), (x2, y2))
 *************************************************/
float Triangle::calcStepY(
        float *vertexValues, 
        float invDenominator) {
    float result;

    result = (vertexValues[2]      - vertexValues[0]) 
           * (this->vertices[1]->x - this->vertices[0]->x) -
             (this->vertices[2]->x - this->vertices[0]->x) 
           * (vertexValues[1]      - vertexValues[0]);

    return result * invDenominator;
}

float Triangle::calcMinX(void) const {
    return min(this->vertices[0]->x, min(this->vertices[1]->x, this->vertices[2]->x));
}

float Triangle::calcMaxX(void) const {
    return max(this->vertices[0]->x, max(this->vertices[1]->x, this->vertices[2]->x));
}

Point *Triangle::getYmin(void) const {
    return this->vertices[0];
}

Point *Triangle::getYmid(void) const {
    return this->vertices[1];
}

Point *Triangle::getYmax(void) const {
    return this->vertices[2];
}

float Triangle::getDepth(int pointIndex) const {
    return this->depth[pointIndex];
}

float Triangle::getDepthStepX(void) const {
    return this->depthStepX;
}

float Triangle::getDepthStepY(void) const {
    return this->depthStepY;
}

float Triangle::getLightAmount(int pointIndex) const {
    return this->lightAmount[pointIndex];
}

float Triangle::getLightAmountStepX(void) const {
    return this->lightAmountStepX;
}

float Triangle::getLightAmountStepY(void) const {
    return this->lightAmountStepY;
}
