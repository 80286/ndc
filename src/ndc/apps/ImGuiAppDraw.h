#ifndef IMGUIAPPDRAW_H
#define IMGUIAPPDRAW_H

#include "apps/ImGuiAppSettings.h"
#include "imgui.h"

class ImGuiAppDraw {
public:
    void Draw(ImGuiAppSettings* settings);
};

#endif
