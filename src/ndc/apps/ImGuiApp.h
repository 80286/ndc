#ifndef MAINAPP_H
#define MAINAPP_H

#include <SDL.h>
#include <thread>
#include <memory>

#include "apps/AbstractApp.h"
#include "apps/ImGuiAppDraw.h"
#include "Renderer.h"
#include "imgui.h"


class ImGuiApp : public AbstractApp
{
public:
    void Run(int width, int height) override;
    void Draw() override;
    void Quit() override;

private:
    const ImVec4 m_clear_color = ImVec4(0.0f, 0.0f, 0.0f, 1.00f);
    const int m_windowWidth = 1280;
    const int m_windowHeight = 720;

    bool m_running = false;

    std::thread m_thread;
    SDL_Window* m_window;

    ImGuiAppDraw m_uiDrawer;
    Renderer m_renderer;

    std::shared_ptr<Drawer> m_drawer;
    std::shared_ptr<ImGuiAppSettings> m_settings;

    void ui_thread(int width, int height);
};

#endif
