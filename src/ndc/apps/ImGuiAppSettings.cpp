#include "apps/ImGuiAppSettings.h"

template<class T>
static void SetDefaultArrayValues(const std::map<T, FloatField>& fields, float* array)
{
    for(const auto field: fields)
    {
        array[field.first] = field.second.defaultValue;
    }
}

void ImGuiAppSettings::SetDefaultValues()
{
    m_frustumFunction = 0;
    m_fillType = 0;

    SetDefaultArrayValues(NdcConstants::Model(), m_modelValues);
    SetDefaultArrayValues(NdcConstants::View(), m_viewValues);
    SetDefaultArrayValues(NdcConstants::GlFrustum(), m_projectionValues);
    SetDefaultArrayValues(NdcConstants::GluPerspective(), m_gluPerspectiveValues);
}

ImGuiAppSettings::ImGuiAppSettings()
{
    SetDefaultValues();
}
