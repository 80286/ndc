#include "apps/ImGuiApp.h"

#include <iostream>
#include <stdexcept>
#include <cstdint>

#include "imgui_impl_sdl.h"
#include "imgui_impl_sdlrenderer.h"


struct {
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture = NULL;
    SDL_Window* window = NULL;

    int32_t* pixels = NULL;

    void Init(const int windowWidth, const int windowHeight) {
        // Setup SDL
        // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
        // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
        {
            printf("Error: %s\n", SDL_GetError());
            return;
        }

        // Setup window
        SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
        window = SDL_CreateWindow("Dear ImGui SDL2+SDL_Renderer example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, window_flags);

        // Setup SDL_Renderer instance
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
        if(renderer == NULL) {
            SDL_Log("Error creating SDL_Renderer!");
            return;
        }

        texture = SDL_CreateTexture(renderer,
            SDL_PIXELFORMAT_ARGB8888,
            SDL_TEXTUREACCESS_STREAMING,
            windowWidth, windowHeight);

        SDL_RendererInfo info;
        SDL_GetRendererInfo(renderer, &info);
        SDL_Log("Current SDL_Renderer: %s; flags: %d", info.name, info.flags);

        pixels = new int32_t[windowWidth * windowHeight];
    }

    void Destroy() {
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);

        delete [] pixels;
    }
} threadContext;

void ImGuiApp::Run(int width, int height) {
    if(m_running) {
        return;
    }

    Quit(); // Join thread brefore starting again if the window was closed by the user from the renderer thread. 

    m_running = true;
    m_settings = std::make_shared<ImGuiAppSettings>();
    m_thread = std::thread(&ImGuiApp::ui_thread, this, width, height);

    while(m_running)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

void ImGuiApp::Quit() { 
    m_running = false; 
    m_settings.reset();

    if(m_thread.joinable()) {
        m_thread.join();
    }
}

/* RenderCallback is modified version of ImGui_ImplSDLRenderer_RenderDrawData function.
 * Usage of SDL_RenderSetClipRect has been removed here due to rendering problems. */
static void RenderCallback(const ImDrawList* parent_list, const ImDrawCmd* cmd)
{
    ImDrawData* drawData = (ImDrawData*)cmd->UserCallbackData;

    const ImDrawList* cmd_list = parent_list;
    const ImDrawVert* vtx_buffer = cmd_list->VtxBuffer.Data;
    const ImDrawIdx* idx_buffer = cmd_list->IdxBuffer.Data;

    const float* xy = (const float*)((const char*)vtx_buffer + IM_OFFSETOF(ImDrawVert, pos));
    const float* uv = (const float*)((const char*)vtx_buffer + IM_OFFSETOF(ImDrawVert, uv));
    const int* color = (const int*)((const char*)vtx_buffer + IM_OFFSETOF(ImDrawVert, col));
    
    const ImDrawCmd* pcmd = cmd;

    // Bind texture, Draw
    SDL_Texture* tex = (SDL_Texture*)pcmd->GetTexID();
    SDL_RenderGeometryRaw(threadContext.renderer, tex,
        xy, (int)sizeof(ImDrawVert),
        color, (int)sizeof(ImDrawVert),
        uv, (int)sizeof(ImDrawVert),
        cmd_list->VtxBuffer.Size,
        idx_buffer + pcmd->IdxOffset, pcmd->ElemCount, sizeof(ImDrawIdx));
}

static void FixDrawData(ImDrawData* drawData)
{
    for(int n = 0; n < drawData->CmdListsCount; n++)
    {
        ImDrawList* cmd_list = drawData->CmdLists[n];

        for(int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];

            pcmd->UserCallback = RenderCallback;
            pcmd->UserCallbackData = drawData;
        }
    }
}

void ImGuiApp::Draw()
{
    m_uiDrawer.Draw(m_settings.get());
}

void ImGuiApp::ui_thread(int width, int height)
{
    threadContext.Init(m_windowWidth, m_windowHeight);

    m_drawer = std::make_unique<Drawer>((uint8_t*)threadContext.pixels, m_windowWidth, m_windowHeight, 4);

    m_renderer.setDrawer(m_drawer);
    m_renderer.setSettings(m_settings);
    m_renderer.Init();

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForSDLRenderer(threadContext.window);
    ImGui_ImplSDLRenderer_Init(threadContext.renderer);

    // Main loop
    bool done = false;
    while(!done) {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if(event.type == SDL_QUIT)
                done = true;
            if(event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(threadContext.window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplSDLRenderer_NewFrame();
        ImGui_ImplSDL2_NewFrame(threadContext.window);
        ImGui::NewFrame();

        Draw();

        // Rendering
        ImGui::Render();

        SDL_SetRenderDrawColor(threadContext.renderer, (Uint8)(m_clear_color.x * 255), (Uint8)(m_clear_color.y * 255), (Uint8)(m_clear_color.z * 255), (Uint8)(m_clear_color.w * 255));
        SDL_RenderClear(threadContext.renderer);

        m_renderer.draw();

        SDL_UpdateTexture(threadContext.texture, NULL, threadContext.pixels, m_windowWidth * sizeof(Uint32));
        SDL_RenderCopy(threadContext.renderer, threadContext.texture, NULL, NULL);

        const auto drawData = ImGui::GetDrawData();

        FixDrawData(drawData);

        ImGui_ImplSDLRenderer_RenderDrawData(drawData);

        SDL_RenderPresent(threadContext.renderer);
    }

    // Cleanup
    ImGui_ImplSDLRenderer_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    threadContext.Destroy();

    m_running = false;

    SDL_Quit();
}
