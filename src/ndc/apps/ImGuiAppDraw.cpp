#include "ImGuiAppDraw.h"

static const char* comboValuesFrustum[] = { "glFrustum()", "gluPerspective()" };
static const char* comboValuesDrawMode[] = { "Filled", "Wired" };
static const ImGuiWindowFlags childFlags = ImGuiWindowFlags_HorizontalScrollbar;

static void AddMatrix(const char* label, const char* tableId, const int size, const Matrix4& m) {
    static ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg;
    const int cellWidth = 240;

    ImGui::BeginChild(tableId, ImVec2(0.27f * cellWidth * size, size * 26.0f), false, childFlags);
    ImGui::Text("%s", label);

    if(ImGui::BeginTable(tableId, size, flags, ImVec2(cellWidth, 0.0f))) {
        for(int row = 0; row < size; row++) {
            ImGui::TableNextRow();

            for(int column = 0; column < size; column++) {
                ImGui::TableSetColumnIndex(column);

                char buf[32];
                sprintf(buf, "%3.3f", m.get(row, column));

                ImGui::TextUnformatted(buf);
            }
        }

        ImGui::EndTable();
    }

    ImGui::EndChild();
}

template<class T>
static void AddSpinners(const std::map<T, FloatField>& fields, float* values, const char* title, const int width, const int height) {
    ImGui::BeginChild(title, ImVec2(width, height), false, childFlags);
    ImGui::Text("%s", title);

    for(const auto& field: fields) {
        ImGui::SliderFloat(field.second.label.c_str(), &values[field.first], field.second.min, field.second.max, "%.3f");
    }

    ImGui::EndChild();
}

static void AddComboPanel(const char* title, const char** items, const int itemsLen, int* valueRef, const float width) {
    ImGui::BeginChild(title, ImVec2(width, 40), false, childFlags);

    for(int i = 0; i < itemsLen; ++i) {
        ImGui::RadioButton(items[i], valueRef, i);
        ImGui::SameLine();
    }

    ImGui::EndChild();
}

void ImGuiAppDraw::Draw(ImGuiAppSettings* settings) {
    /* ImGui::ShowDemoWindow(); */

    ImGui::Begin("ndc");
    const int n = 4;

    AddMatrix("Model matrix", "a", n, settings->m_modelMatrix);
    ImGui::SameLine();
    AddMatrix("View matrix", "b", n, settings->m_viewMatrix);
    ImGui::SameLine();
    AddMatrix("Projection matrix", "c", n, settings->m_projectionMatrix);

    const float width = ImGui::GetContentRegionAvail().x * 0.2f;
    AddSpinners(NdcConstants::Model(), settings->m_modelValues, "Model", width, 240);
    ImGui::SameLine();
    AddSpinners(NdcConstants::Model(), settings->m_viewValues, "View", width, 240);
    ImGui::SameLine();
    AddSpinners(NdcConstants::GlFrustum(), settings->m_projectionValues, "glFrustum()", width, 190);
    ImGui::SameLine();
    AddSpinners(NdcConstants::GluPerspective(), settings->m_gluPerspectiveValues, "gluPerspective()", width, 120);

    AddComboPanel("Frustum function", comboValuesFrustum, 2, &settings->m_frustumFunction, 260.0f);
    ImGui::SameLine();
    AddComboPanel("Draw mode", comboValuesDrawMode, 2, &settings->m_fillType, 150.0f);
    ImGui::SameLine();

    if(ImGui::Button("Restore default values")) {
        settings->SetDefaultValues();
    }

    ImGui::End();
}
