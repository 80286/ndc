#ifndef ABSTRACTAPP_H
#define ABSTRACTAPP_H


class AbstractApp
{
public:
    virtual void Run(int width, int height) = 0;
    virtual void Draw() = 0;
    virtual void Quit() = 0;
};

#endif
