#ifndef IMGUIAPPSETTINGS_H
#define IMGUIAPPSETTINGS_H

#include "Constants.h"
#include "AbstractSettings.h"

class ImGuiAppSettings : public AbstractSettings
{
public:
    ImGuiAppSettings();

    void SetDefaultValues();

    float m_modelValues[MT_SIZE];
    float m_viewValues[MT_SIZE];
    float m_projectionValues[FT_SIZE];
    float m_gluPerspectiveValues[GPT_SIZE];

    int m_frustumFunction;
    int m_fillType;

    float view_sx() override { return m_viewValues[MT_SX]; }
    float view_sy() override { return m_viewValues[MT_SY]; }
    float view_sz() override { return m_viewValues[MT_SZ]; }
    float view_tx() override { return m_viewValues[MT_TX]; }
    float view_ty() override { return m_viewValues[MT_TY]; }
    float view_tz() override { return m_viewValues[MT_TZ]; }
    float view_rotx() override { return m_viewValues[MT_ROTX]; }
    float view_roty() override { return m_viewValues[MT_ROTY]; }
    float view_rotz() override { return m_viewValues[MT_ROTZ]; }

    float model_sx() override { return m_modelValues[MT_SX]; }
    float model_sy() override { return m_modelValues[MT_SY]; }
    float model_sz() override { return m_modelValues[MT_SZ]; }
    float model_tx() override { return m_modelValues[MT_TX]; }
    float model_ty() override { return m_modelValues[MT_TY]; }
    float model_tz() override { return m_modelValues[MT_TZ]; }
    float model_rotx() override { return m_modelValues[MT_ROTX]; }
    float model_roty() override { return m_modelValues[MT_ROTY]; }
    float model_rotz() override { return m_modelValues[MT_ROTZ]; }

    float frustum_left() override   { return m_projectionValues[FT_LEFT]; }
    float frustum_right() override  { return m_projectionValues[FT_RIGHT]; };
    float frustum_bottom() override { return m_projectionValues[FT_BOTTOM]; };
    float frustum_top() override    { return m_projectionValues[FT_TOP]; };
    float frustum_near() override   { return m_projectionValues[FT_NEAR]; };
    float frustum_far() override    { return m_projectionValues[FT_FAR]; };

    float frustum2_fov() override  { return m_gluPerspectiveValues[GPT_FOVY]; };
    float frustum2_aspect() override { return m_gluPerspectiveValues[GPT_ASPECT]; };
    float frustum2_near() override { return m_gluPerspectiveValues[GPT_NEAR]; };
    float frustum2_far() override  { return m_gluPerspectiveValues[GPT_FAR]; };

    void setFillMode(bool enabled) override { m_fillType = enabled ? 1 : 0; }
    bool isFillModeEnabled() override { return m_fillType == 0; }
    bool isGlFrustumEnabled() override { return m_frustumFunction == 0; }
};

#endif
