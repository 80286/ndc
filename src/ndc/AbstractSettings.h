#ifndef ABSTRACT_SETTINGS_H
#define ABSTRACT_SETTINGS_H

#include "math/Matrix4.h"

class AbstractSettings {
public:
    Matrix4 m_projectionMatrix;
    Matrix4 m_viewMatrix;
    Matrix4 m_modelMatrix;

    void updateProjectionMatrix();
    void updateViewMatrix();
    void updateModelMatrix();

    virtual float view_sx() = 0;
    virtual float view_sy() = 0;
    virtual float view_sz() = 0;
    virtual float view_tx() = 0;
    virtual float view_ty() = 0;
    virtual float view_tz() = 0;
    virtual float view_rotx() = 0;
    virtual float view_roty() = 0;
    virtual float view_rotz() = 0;

    virtual float model_sx() = 0;
    virtual float model_sy() = 0;
    virtual float model_sz() = 0;
    virtual float model_tx() = 0;
    virtual float model_ty() = 0;
    virtual float model_tz() = 0;
    virtual float model_rotx() = 0;
    virtual float model_roty() = 0;
    virtual float model_rotz() = 0;

    virtual float frustum_left() = 0;
    virtual float frustum_right() = 0;
    virtual float frustum_bottom() = 0;
    virtual float frustum_top() = 0;
    virtual float frustum_near() = 0;
    virtual float frustum_far() = 0;

    virtual float frustum2_fov() = 0;
    virtual float frustum2_aspect() = 0;
    virtual float frustum2_near() = 0;
    virtual float frustum2_far() = 0;

    virtual void setFillMode(bool enabled) = 0;
    virtual bool isFillModeEnabled() = 0;
    virtual bool isGlFrustumEnabled() = 0;
};

#endif
