#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <map>
#include <string>

enum FrustumType {
    FT_LEFT = 0,
    FT_RIGHT = 1,
    FT_BOTTOM = 2,
    FT_TOP = 3,
    FT_NEAR = 4,
    FT_FAR = 5,
    FT_SIZE = 6
};

enum GluPerspectiveType {
    GPT_FOVY = 0,
    GPT_ASPECT = 1,
    GPT_NEAR = 2,
    GPT_FAR = 3,
    GPT_SIZE = 4
};

enum ModelType {
    MT_ROTX = 0,
    MT_ROTY = 1,
    MT_ROTZ = 2,
    MT_TX = 3,
    MT_TY = 4,
    MT_TZ = 5,
    MT_SX = 6,
    MT_SY = 7,
    MT_SZ = 8,
    MT_SIZE = 9
};

struct FloatField
{
    float min;
    float max;
    float defaultValue;
    std::string label;
};

namespace NdcConstants
{
    const std::map<FrustumType, FloatField>& GlFrustum();
    const std::map<GluPerspectiveType, FloatField>& GluPerspective();
    const std::map<ModelType, FloatField>& Model();
    const std::map<ModelType, FloatField>& View();
}

#endif
