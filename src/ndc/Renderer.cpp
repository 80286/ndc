#include "Renderer.h"

Renderer::Renderer(void) : 
        m_debugEnabled(false) {
    mdlv6_init(&(m_mdl));
}

void Renderer::Init() {
    readModel();
    updateViewMatrix();
}

Renderer::~Renderer(void) {
    mdlv6_free(&(m_mdl));
}

void Renderer::setSettings(std::shared_ptr<AbstractSettings> settings) {
    m_settings = settings;
}

void Renderer::setDrawer(std::shared_ptr<Drawer> newDrawer) {
    m_drawer = newDrawer;
}

std::shared_ptr<Drawer> Renderer::getDrawer(void) {
    return m_drawer;
}

void Renderer::setFillColor(uint8_t r, uint8_t g, uint8_t b) {
    m_fillColor.set(r, g, b);
}

void Renderer::setWiredMode(void) {
    m_settings->setFillMode(false);
}

void Renderer::setFilledMode(void) {
    m_settings->setFillMode(true);
}

void Renderer::updateViewMatrix(void) {
    m_settings->updateViewMatrix();
}

void Renderer::updateModelMatrix(void) {
    m_settings->updateModelMatrix();
}

void Renderer::computeMVP() {
    const auto& viewMatrix = m_settings->m_viewMatrix;
    const auto& modelMatrix = m_settings->m_modelMatrix;
    const auto& projectionMatrix = m_settings->m_projectionMatrix;

    /* MVP = (Projection * View * Model) matrix */
    const Matrix4 modelView = viewMatrix * modelMatrix;

    m_mvpMatrix = projectionMatrix * modelView;
}

void Renderer::updateProjectionMatrixFrustum(void) {
    m_settings->updateProjectionMatrix();
}

void Renderer::updateProjectionMatrixFov(void) {
    m_settings->updateProjectionMatrix();
}

void Renderer::drawAxis(void) {
    Vector4 axisBegin;
    Vector4 axisEnd;

    updateViewMatrix();

    /* Model matrix is ignored: */
    m_settings->m_modelMatrix.loadIdentity();

    /* Prepare (Projection * View) matrix: */
    computeMVP();

    const float axisLen = 64.0f;

    /* X axis: */
    setFillColor(0, 255, 0);
    axisBegin.set(-axisLen, 0.0f, 0.0f, 1.0f);
    axisEnd.set  ( axisLen, 0.0f, 0.0f, 1.0f);
    transformAndDrawSegment(axisBegin, axisEnd);

    /* Y axis: */
    setFillColor(0, 0, 255);
    axisBegin.set(0.0f, -axisLen, 0.0f, 1.0f);
    axisEnd.set  (0.0f,  axisLen, 0.0f, 1.0f);
    transformAndDrawSegment(axisBegin, axisEnd);

    /* Z axis: */
    setFillColor(255, 0, 0);
    axisBegin.set(0.0f, 0.0f, -axisLen, 1.0f);
    axisEnd.set  (0.0f, 0.0f,  axisLen, 1.0f);
    transformAndDrawSegment(axisBegin, axisEnd);
}

void Renderer::drawGrid(void) {
    /*
     * -N < X < N
     *      Y = 0
     * -N < Z < N
     */
    Vector4 segmentBegin, segmentEnd;

    const float gridLength = 64.0f;
    const float gridStep = 8.0f;
    const float gridY = 0.0f;

    updateViewMatrix();
    m_settings->m_modelMatrix.loadIdentity();
    computeMVP();
    setFillColor(100, 100, 100);

    for(float i = -gridLength; i <= gridLength; i += gridStep) {
        /* X: -N -> N */
        segmentBegin.set(i, gridY, -gridLength, 1.0f);
        segmentEnd.set  (i, gridY,  gridLength, 1.0f);
        transformAndDrawSegment(segmentBegin, segmentEnd);

        /* Z: -N -> N */
        segmentBegin.set(-gridLength, gridY, i, 1.0f);
        segmentEnd.set  ( gridLength, gridY, i, 1.0f);
        transformAndDrawSegment(segmentBegin, segmentEnd);
    }
}

Vector4 Renderer::transformVector4(const Vector4 &v) {
    return (m_mvpMatrix * v);
}

void Renderer::transformPointToViewport(Point &out) {
    out.viewportTransform(
            m_drawer->getWidth() - 1,
            m_drawer->getHeight() - 1);
}

void Renderer::drawSegment(Point &a, Point &b) {
    transformPointToViewport(a);
    transformPointToViewport(b);

    m_drawer->drawLine(a, b, m_fillColor);
}

void Renderer::drawTriangle(Point &a, Point &b, Point &c) {
    transformPointToViewport(a);
    transformPointToViewport(b);
    transformPointToViewport(c);

    setFillColor(255, 255, 255);

    if (m_settings->isFillModeEnabled()) {
        m_drawer->fillTriangle(a, b, c);
    } else {
        m_drawer->drawWiredTriangle(a, b, c, m_fillColor);
    }
}

void Renderer::drawPolygon(std::vector <Point> &points) {
    for(int i = 1; i < points.size() - 1; ++i) {
        /* (i, i+1, first) */
        drawTriangle(points[0], points[i], points[i + 1]);
    }
}

void printPoints(const char *prefix, std::vector <Point> &points) {
    printf("%s\n", prefix);

    for(auto it = points.begin(); it != points.end(); it++) {
        (*it).pos.print();
    }
}

void Renderer::transformAndDrawTriangle(
        Vector4 va, 
        Vector4 vb, 
        Vector4 vc, 
        Vector3 *normals) {
    Point a(transformVector4(va), normals[0]);
    Point b(transformVector4(vb), normals[1]);
    Point c(transformVector4(vc), normals[2]);

    if(a.pos.isInsideFrustum() 
     && b.pos.isInsideFrustum() 
     && c.pos.isInsideFrustum()) {
        drawTriangle(a, b, c);
        return;
    }

    std::vector <Point> triangle;
    triangle.push_back(a);
    triangle.push_back(b);
    triangle.push_back(c);

    const bool isClippedTriangle = clipPolygon(triangle);

    if(isClippedTriangle) {
        drawPolygon(triangle);
    }
}

void Renderer::transformAndDrawSegment(Vector4 &va, Vector4 &vb) {
    Point a;
    Point b;

    a.pos = transformVector4(va);
    b.pos = transformVector4(vb);

    if(a.pos.isInsideFrustum() && b.pos.isInsideFrustum()) {
        drawSegment(a, b);
        return;
    }

    const bool isClippedSegment = clipSegment(a.pos, b.pos);

    if(isClippedSegment) {
        drawSegment(a, b);
    }
}

bool Renderer::clipSegmentCoordToPlane(Vector4 &a, Vector4 &b, const int coordIndex, const float sideFactor) {
    const float aCoord = sideFactor * a[coordIndex];
    const float bCoord = sideFactor * b[coordIndex];

    const bool aInside = (aCoord <= a.w());
    const bool bInside = (bCoord <= b.w());

    if(aInside && bInside) {
        return true;
    }

    const float t1 = (a.w() - aCoord);
    const float t2 = (b.w() - bCoord);
    const float t = t1 / (t1 - t2);

    if(!aInside && bInside) {
        /* a := a + t(b - a); */
        a.setRayPoint(a, b, t);
        return true;
    } else if(aInside && !bInside) {
        /* b := a + t(b - a); */
        b.setRayPoint(a, b, t);
        return true;
	}

    return false;
}

bool Renderer::clipSegmentCoord(Vector4 &a, Vector4 &b, const int coordIndex) {
    return clipSegmentCoordToPlane(a, b, coordIndex,  1.0f)
        && clipSegmentCoordToPlane(a, b, coordIndex, -1.0f);
}

bool Renderer::clipSegment(Vector4 &a, Vector4 &b) {
    return clipSegmentCoord(a, b, 0)
        && clipSegmentCoord(a, b, 1)
        && clipSegmentCoord(a, b, 2);
}

void Renderer::clipPolygonCoordToPlane(
        std::vector <Point> &result,
        std::vector <Point> &points, 
        const int coordIndex, 
        const float sideFactor) {
    float previousCoord, currentCoord;
    bool previousInside, currentInside;
    float t, t1, t2;
    Point *previous;
    Point* current;
    Point clipped;

    result.clear();

    previous = &(points[points.size() - 1]);
    previousCoord = sideFactor * previous->pos[coordIndex];
    previousInside = (previousCoord <= previous->pos.w());

    for(auto it = points.begin(); it != points.end(); it++) {
        current = &(*it);
        currentCoord = sideFactor * current->pos[coordIndex];
        currentInside = (currentCoord <= current->pos.w());

        if(previousInside ^ currentInside) {
            t1 = (previous->pos.w() - previousCoord);
            t2 = (current->pos.w()  - currentCoord);
            t = t1 / (t1 - t2);

            /* 
             * Interp prev scaling it by t: 
             * clipped := prev + t * (curr - prev); 
             * (Interpolate position and normal vector of $prev point)
             * */
            clipped.lerp(*previous, *current, t);
            result.push_back(clipped);
        }

        if (currentInside) {
            result.push_back(*current);
        }

        previous = current;
        previousCoord = currentCoord;
        previousInside = currentInside;
    }
}

bool Renderer::clipPolygonCoord(std::vector <Point> &points, int coordIndex) {
    std::vector <Point> clippedPoints;

    /* Clip points to +[XYZ] plane: */
    clipPolygonCoordToPlane(clippedPoints, points, coordIndex, 1.0f);

    if (clippedPoints.empty()) {
        /* All points lies behind plane. */
        return false;
    }

    /* Clip points to -[XYZ] plane: */
    clipPolygonCoordToPlane(points, clippedPoints, coordIndex, -1.0f);

    return !points.empty();
}

bool Renderer::clipPolygon(std::vector <Point> &points) {
    if(clipPolygonCoord(points, 0)
    && clipPolygonCoord(points, 1)
    && clipPolygonCoord(points, 2)) {
        return true;
    }

    return false;
}

void Renderer::renderModelFrame(int frameId) {
    mdlv6_vertex_t *frame_vertices;
    mdlv6_vertex_t *v1ptr, *v2ptr, *v3ptr;
    int v1index, v2index, v3index;
    mdlv6_triangle_t *triangle;
    Vector4 va, vb, vc;
    Vector3 normals[3];

    if (frameId < 0 || frameId > m_mdl.header.frames_len - 1) {
        fprintf(stderr, "renderModelFrame(); Mdlv6 wrong frameId=%d.\n", frameId);
        return;
    }

    updateViewMatrix();
    updateModelMatrix();
    computeMVP();

    for(int i = 0; i < m_mdl.header.triangles_len; i++) {
        frame_vertices = m_mdl.frames[frameId].frame.vertices;
        triangle = m_mdl.triangles + i;

        v1index = triangle->vertex[0];
        v2index = triangle->vertex[1];
        v3index = triangle->vertex[2];

        v1ptr = frame_vertices + v1index;
        v2ptr = frame_vertices + v2index;
        v3ptr = frame_vertices + v3index;

        mdlv6_transformVertex(va.array, &(m_mdl.header), v1ptr);
        mdlv6_transformVertex(vb.array, &(m_mdl.header), v2ptr);
        mdlv6_transformVertex(vc.array, &(m_mdl.header), v3ptr);

        va.array[3] = 1.0f;
        vb.array[3] = 1.0f;
        vc.array[3] = 1.0f;

        mdlv6_getNormal(normals[0].array, v1ptr->normal_index);
        mdlv6_getNormal(normals[1].array, v2ptr->normal_index);
        mdlv6_getNormal(normals[2].array, v3ptr->normal_index);

        transformAndDrawTriangle(va, vb, vc, normals);
    }
}

void Renderer::draw(void) {
    /* printf("Renderer::draw(); Viewport(w=%d, h=%d, fill=%s);\n", */ 
    /*         m_drawer->getWidth(), */ 
    /*         m_drawer->getHeight(), */
    /*         m_settings->isFillModeEnabled() ? "y" : "n"); */

    updateProjectionMatrixFrustum();

    m_drawer->clear();
    m_drawer->initDepthBuffer();

    drawGrid();
    drawAxis();

    /* draw3DTriangle(); */
    /* draw3DPyramid(); */

    renderModelFrame(0);
}

void Renderer::draw3DPyramid(void) {
    Vector3 normals[3];

    updateViewMatrix();
    updateModelMatrix();
    computeMVP();

    normals[0].set(-0.525731f,  0.000000f,  0.850651f);
    normals[1].set( 0.525731f,  0.000000f,  0.850651f);
    normals[2].set(-0.716567f,  0.681718f,  0.147621f);

    /* 3D Pyramid: */
    Vector3 A(-10.0f,    -10.0f,  10.0f);
    Vector3 B( 10.0f,    -10.0f,  10.0f);
    Vector3 C(  0.0f,    -10.0f, -10.0f);
    Vector3 D(  0.0f,     10.0f,   0.0f);
    transformAndDrawTriangle(A, C, D, normals);
    transformAndDrawTriangle(B, C, D, normals);
    transformAndDrawTriangle(A, B, D, normals);
}

void Renderer::draw3DTriangle(void) {
    Vector3 normals[3];

    updateViewMatrix();
    updateModelMatrix();
    computeMVP();

    normals[0].set(-0.525731f,  0.000000f,  0.850651f);
    normals[1].set( 0.525731f,  0.000000f,  0.850651f);
    normals[2].set(-0.716567f,  0.681718f,  0.147621f);

    /* 3D Triangle: */
    Vector4 A(-20.0f, 10.0f, 10.0f, 1.0f);
    Vector4 B( 20.0f, 10.0f, 10.0f, 1.0f);
    Vector4 C(  0.0f, 60.0f, 10.0f, 1.0f);
    transformAndDrawTriangle(A, B, C, normals);
}

void Renderer::readModel(void) {
    const char *model_path = "../assets/turrbase.mdl";

    if(readModel(model_path) == 0) {
        printf("Model '%s' opened.\n", model_path);
    }
}

int Renderer::readModel(const char *filename) {
    return mdlv6_readfile(filename, &(m_mdl));
}
