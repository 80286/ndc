#ifndef DRAWER_H
#define DRAWER_H

#include <math.h>
#include <iostream>
#include <stdint.h>

#include "math/common.h"
#include "math/Edge.h"
#include "math/Point.h"
#include "math/Triangle.h"
#include "RGB.h"

class Drawer {
public:
     Drawer(uint8_t *, const int, const int, const int);
    ~Drawer(void);

    uint8_t *getData(void);
    float getAspect(void) const;
    int   getWidth(void) const;
    int   getHeight(void) const;

    void clear(void);
    int  getPixelPos(int, int);
    uint8_t *getPixelPtr(int, int);
    bool isInScreen(int, int) const;

    void drawPixel(int, int, const RGB &);
    void drawPixel(int, int, uint8_t, uint8_t, uint8_t);
    void drawPixel(Point, int, int);

    void drawPoint(int, int, uint8_t, uint8_t, uint8_t);
    void drawPoint(int, int, const RGB &);
    void drawPoint(Point, const RGB &);

    void drawLine(Point, Point, const RGB &);
    void drawLine(int, int, int, int, const RGB &);
    void drawHorizontalLine(int, int, int, const RGB &);

    void initDepthBuffer(void);
    void drawWiredTriangle(Point, Point, Point, const RGB &);

    void fillTriangle(Point &, Point &, Point &);
    void fillSubTriangle(Triangle *, Edge *, Edge *, bool);
    void fillHorizontalLine(Triangle *, Edge *, Edge *, int);

private:
    uint8_t *m_img;
    int m_width;
    int m_height;
    int m_pixelSize;
    const uint8_t m_defaultAlpha = 255;

    float *m_depthBuffer;
};


#endif
